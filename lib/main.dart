import 'package:fdi/blocs/BlocUser.dart';
import 'package:fdi/restart_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'screens/layout_screen.dart';
import 'package:foo/foo.dart';

// import 'pages/Layout.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isAuthendticated = prefs.getBool("IsAuthenticated") ?? false;
  String userId = prefs.getString("userid");
  runApp(
    RestartWidget(
        child: MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Layout(
        isAuthendticated: isAuthendticated,
      ),
    )),
    // Foo(
    //   app: MaterialApp(
    //     debugShowCheckedModeBanner: false,
    //     home: Layout(
    //       isAuthendticated: isAuthendticated,
    //     ),
    //   ),
    // ),

    // RestartWidget(
    //   child: MaterialApp(
    //     debugShowCheckedModeBanner: false,
    //     home: Layout(
    //       isAuthendticated: isAuthendticated,
    //     ),
    //   ),
    // ),
  );
}
