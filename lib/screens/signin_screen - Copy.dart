import 'package:fdi/blocs/login_bloc.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:fdi/screens/forgot_password_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:foo/foo.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'register_screen.dart';

class SigninScreen extends StatefulWidget {
  SigninScreen({Key key}) : super(key: key);

  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  TextEditingController emailEditingController = TextEditingController();
  TextEditingController passEditingController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool valPassword = false;
  bool isLoading = false;
  bool valEmail = false;
  bool notValid = false;
  bool _obscureText = true;

  _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  _setInputEmailError(String email) {
    setState(() {
      valEmail = email.isEmpty ? true : false;
    });
  }

  _setInputPAsswordError(String password) {
    setState(() {
      valPassword = password.isEmpty ? true : false;
    });
  }

  Future<void> _login() async {
    _onLoading();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isLoading = true;
    await Future.delayed(const Duration(milliseconds: 2000));
    loginBloc
        .login(emailEditingController.text, passEditingController.text)
        .then((result) => {
              if (result) {showAlertDialog(true)} else {showAlertDialog(false)}
            });
    // loginStream.listen((data) {
    //   print("DataReceived: " + data);
    // }, onDone: () {
    //   print("Task Done");
    // }, onError: (error) {
    //   print("Some Error");
    // });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Color(0xff5EAF1FB),
      body: Stack(
        children: [
          // Container(
          //   height: height,
          // ),
          Positioned(
            top: width * 0.3,
            right: 0.0,
            left: 0.0,
            child: Form(
              key: _formKey,
              child: Container(
                margin: EdgeInsets.all(width * 0.05),
                padding: EdgeInsets.all(width * 0.05),
                decoration: BoxDecoration(color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Alamat Email  / Username",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: width * 0.01),
                      child: TextFormField(
                        controller: emailEditingController,
                        validator: (value) => _setInputEmailError(value),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xff03A9F4))),
                          contentPadding: EdgeInsets.all(10.0),
                          hintText: "Masukan email Anda",
                        ),
                      ),
                    ),
                    SizedBox(
                      height: width * 0.05,
                    ),
                    Text(
                      "Password",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: width * 0.01),
                      child: TextFormField(
                        obscureText: _obscureText,
                        controller: passEditingController,
                        validator: (value) => _setInputPAsswordError(value),
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Color(0xff03A9F4))),
                            contentPadding: EdgeInsets.all(10.0),
                            hintText: "Masukan password anda",
                            suffixIcon: IconButton(
                              onPressed: _toggle,
                              icon: _obscureText == true
                                  ? Icon(
                                      Icons.visibility,
                                      color: Colors.blue,
                                    )
                                  : Icon(
                                      Icons.visibility_off,
                                      color: Colors.blue,
                                    ),
                            )),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.symmetric(vertical: width * 0.01),
                        child: CheckboxListTile(
                          title: Text('Tetap Login'),
                          value: timeDilation != 1.0,
                          onChanged: (bool value) {
                            setState(() {
                              timeDilation = value ? 3.0 : 1.0;
                            });
                          },
                          secondary: GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (_) {
                                return ForgotPasswordScreen();
                              }));
                            },
                            child: Text(
                              "Lupa password",
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                          controlAffinity: ListTileControlAffinity.leading,
                          contentPadding: EdgeInsets.all(0.0),
                        )),
                    Container(
                        child: ButtonTheme(
                      minWidth: width,
                      height: width * 0.11,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(color: Colors.transparent)),
                        onPressed: () async {
                          _login();
                        },
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Text("SIGN IN"),
                      ),
                    )),
                    SizedBox(
                      height: width * 0.10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Belum memiliki akun? "),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (_) {
                              return RegisterScreen();
                            }));
                          },
                          child: Text(
                            "Daftar sekarang",
                            style: TextStyle(color: Colors.blue),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          // Visibility(
          //   visible: false,
          //   child: Container(
          //       height: height * 1.0,
          //       color: Color.fromRGBO(0, 0, 0, 0.5),
          //       child: Center(
          //         child: buildLoadingWidget(),
          //       )),
          // )
        ],
      ),
    );
  }

  void _navigation() {}

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            height: MediaQuery.of(context).size.height * 0.1,
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new CircularProgressIndicator(
                    backgroundColor: Colors.white,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                new Text("Loading..."),
              ],
            ),
          ),
        );
      },
    );
    new Future.delayed(new Duration(seconds: 3), () {
      Navigator.pop(context); //pop dialog
      // _login();
    });
  }

  showAlertDialog(bool authenticated) {
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Container(
        height: MediaQuery.of(context).size.height * 0.15,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Center(
                child: Column(
                  children: [
                    Icon(
                      authenticated
                          ? Icons.sentiment_dissatisfied
                          : Icons.sentiment_very_dissatisfied,
                      color: authenticated ? Colors.blue : Colors.red,
                      size: 80.0,
                    ),
                    Text(
                      authenticated
                          ? "Anda berhasil login!"
                          : "Email atau password salah",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: authenticated ? Colors.blue : Colors.red),
                      overflow: TextOverflow.clip,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[],
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );

    new Future.delayed(new Duration(seconds: 3), () {
      Navigator.pop(context);
    });
  }
}
