import 'package:fdi/screens/chat_screen.dart';
import 'package:fdi/screens/home_screen.dart';
import 'package:fdi/screens/profile_screen.dart';
import 'package:fdi/screens/signin_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'shared/responsive_widget.dart';

class Layout extends StatefulWidget {
  Layout({Key key, @required this.isAuthendticated}) : super(key: key);
  final bool isAuthendticated;

  @override
  _LayoutState createState() => _LayoutState();
}

class _LayoutState extends State<Layout> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  bool isAuthendticated = false;
  String userId = "";

  loadUsername() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      isAuthendticated = prefs.getBool("IsAuthenticated") ?? false;
      userId = prefs.getString("userid");
    });
  }

  @override
  void initState() {
    super.initState();
    loadUsername();
  }

  @override
  void dispose() {
    super.dispose();
  }

  int _currentIndex = 1;

  _tabs() {
    if (isAuthendticated) {
      return [
        ResponsiveWidget(
          builder: (context, constraints) {
            return ChatScreen();
          },
        ),
        ResponsiveWidget(
          builder: (context, constraints) {
            return HomeScreen();
          },
        ),
        ResponsiveWidget(
          builder: (context, constraints) {
            return ProfileScreen();
          },
        ),
      ];
    } else {
      return [
        ResponsiveWidget(
          builder: (context, constraints) {
            return ChatScreen();
          },
        ),
        ResponsiveWidget(
          builder: (context, constraints) {
            return HomeScreen();
          },
        ),
        ResponsiveWidget(
          builder: (context, constraints) {
            return SigninScreen();
          },
        ),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: SafeArea(
        child: IndexedStack(
          index: _currentIndex,
          children: _tabs(),
        ),
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
            color: Color.fromRGBO(70, 132, 219, 1.0),
            boxShadow: [
              BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.1))
            ]),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
            child: GNav(
                gap: 5,
                activeColor: Colors.white,
                iconSize: 24,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                duration: Duration(milliseconds: 500),
                tabBackgroundColor: Color.fromRGBO(49, 108, 191, 1.0),
                color: Colors.white,
                tabs: [
                  GButton(
                    icon: Icons.chat,
                    text: 'Chatting',
                  ),
                  GButton(
                    icon: Icons.home,
                    text: 'Home',
                  ),
                  GButton(
                    icon: isAuthendticated
                        ? Icons.person_outline_sharp
                        : Icons.login,
                    text: isAuthendticated ? "Profile" : 'Sign in',
                    active: true,
                  ),
                ],
                selectedIndex: _currentIndex,
                onTabChange: (index) {
                  setState(() {
                    _currentIndex = index;
                  });
                }),
          ),
        ),
      ),
    );
  }
}
