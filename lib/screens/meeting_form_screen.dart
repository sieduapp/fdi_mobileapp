import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:fdi/repository/repository.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MeetingForm extends StatefulWidget {
  @override
  MeetingFormState createState() {
    return MeetingFormState();
  }
}

class MeetingFormState extends State<MeetingForm> {
  final Repository _repository = Repository();
  final _formKey = GlobalKey<FormState>();
  TextEditingController meeting_name = TextEditingController();
  TextEditingController meeting_password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Meeting Room'),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ListTile(
              title: Text('Meeting name'),
              subtitle: TextFormField(
                controller: meeting_name,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
            ),
            ListTile(
              title: Text('Meeting Password'),
              subtitle: TextFormField(
                controller: meeting_password,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    _onLoading();
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    var userid = await prefs.getString('userid');
                    dynamic response = await _repository.addMeetingRoom(meeting_name.text, meeting_password.text,userid);
                    if(response){
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    }
                  }
                },
                child: Text('Submit'),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            height: MediaQuery.of(context).size.height * 0.1,
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new CircularProgressIndicator(
                    backgroundColor: Colors.white,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                new Text("Loading..."),
              ],
            ),
          ),
        );
      },
    );
    // new Future.delayed(new Duration(seconds: 3), () {
    //   Navigator.pop(context); //pop dialog
    //   // _login();
    // });
  }
}
