import 'dart:convert';

import 'package:fdi/components/cons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class GamePlayScreen extends StatefulWidget {
  GamePlayScreen({Key key, @required this.gameId}) : super(key: key);
  final String gameId;

  @override
  _GamePlayScreenState createState() => _GamePlayScreenState();
}

class _GamePlayScreenState extends State<GamePlayScreen> {
  InAppWebViewController _webViewController;
  String url = "";
  double progress = 0;
  String fileHtmlContent;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Colors.white,
        // appBar: AppBar(
        //   iconTheme: IconThemeData(
        //       color: Colors.black
        //   ),
        //   title: Text("Power Pop Bubbles",style: TextStyle(color: Colors.black),),
        //   backgroundColor: Colors.white,
        // ),
        body: SingleChildScrollView(
          child: Column(children: [
            Stack(
              children: [
                Container(
                  width: width,
                  height: height * 1.0,
                  margin: EdgeInsets.symmetric(
                      horizontal: width * 0.01, vertical: height * 0.01),
                  child: InAppWebView(
                    initialUrl: domain + "termofcondition/${widget.gameId}",
                    // initialData: InAppWebViewInitialData(data: fileHtmlContent),
                    initialOptions: InAppWebViewGroupOptions(
                        crossPlatform: InAppWebViewOptions(
                            javaScriptEnabled: true,
                            debuggingEnabled: true,
                            javaScriptCanOpenWindowsAutomatically: true)),
                    onWebViewCreated: (InAppWebViewController controller) {
                      _webViewController = controller;
                      // _loadHtmlFromAsset();
                    },
                    onLoadStart:
                        (InAppWebViewController controller, String url) {
                      setState(() {
                        this.url = url;
                      });
                    },
                    onLoadStop:
                        (InAppWebViewController controller, String url) async {
                      setState(() {
                        this.url = url;
                      });
                    },
                    onProgressChanged:
                        (InAppWebViewController controller, int progress) {
                      setState(() {
                        this.progress = progress / 100;
                      });
                    },
                  ),
                ),
              ],
            ),
          ]),
        ));
  }

  _loadHtmlFromAsset() async {
    // String gameEmbed =
    //     "<div>\r\n<script src=\"https://cdn.htmlgames.com/embed.js?game=StolenArt&amp;bgcolor=white\"></script>\r\n</div>\r\n";
    String filePath = 'html/contentGame.html';
    String filePathjs = 'html/embed.js';
    String fileHtmlContent = await rootBundle.loadString(filePath);
    String fileHtmlContentJs = await rootBundle.loadString(filePathjs);
    //fileHtmlContent = fileHtmlContent.replaceAll('{CONTENT_GAME}', gameEmbed);
    String urlContent = Uri.dataFromString(fileHtmlContent,
            mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
        .toString();
    _webViewController.injectJavascriptFileFromAsset(assetFilePath: filePathjs);
    _webViewController.loadData(data: fileHtmlContent);

    // _webViewController.(source: fileHtmlContentJs);
  }
}
