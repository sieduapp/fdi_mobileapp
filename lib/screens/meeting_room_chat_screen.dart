import 'package:fdi/screens/meeting_room_screnn.dart';
import 'package:flutter/material.dart';

import 'shared/responsive_widget.dart';

class MeetingRoomChatScreen extends StatefulWidget {
  MeetingRoomChatScreen({Key key}) : super(key: key);

  @override
  _MeetingRoomChatScreenState createState() => _MeetingRoomChatScreenState();
}

class _MeetingRoomChatScreenState extends State<MeetingRoomChatScreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
//      appBar: AppBar(
//        iconTheme: IconThemeData(
//            color: Colors.black
//        ),
//        title: Text("Power Pop Bubbles",style: TextStyle(color: Colors.black),),
//        backgroundColor: Colors.white,
//      ),
        body: SingleChildScrollView(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Stack(
          children: [
            Container(
              width: width,
              height: width * 0.7,
              margin: EdgeInsets.symmetric(horizontal: width * 0.01),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('lib/images/game_hot.png'),
                  fit: BoxFit.cover,
                  alignment: Alignment.topCenter,
                ),
//                          borderRadius: BorderRadius.circular(width * 0.04)
              ),
            ),
            Positioned(
              bottom: width * 0.05,
              left: width * 0.05,
              child: Container(
                child: Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(color: Colors.grey),
                      child: Text(
                        "Category 1",
                        style: TextStyle(color: Colors.white),
                      ),
                      padding: EdgeInsets.symmetric(
                          horizontal: width * 0.02, vertical: width * 0.003),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
        Container(
          padding: EdgeInsets.all(width * 0.03),
          color: Color.fromRGBO(245, 242, 242, 1.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_) {
                        return ResponsiveWidget(
                          builder: (context, constraints) {
                            return MeetingRoomScreen();
                          },
                        );
                      }));
                    },
                    child: Icon(Icons.people),
                  ),
                  Text("(20)"),
                ],
              ),
              Row(
                children: [
                  Icon(Icons.chat),
                  Text("(0)"),
                ],
              ),
            ],
          ),
        ),
        Container(
          color: Color.fromRGBO(245, 242, 242, 1.0),
          child: ListView.builder(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: 3,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(bottom: width * 0.02),
                  padding: EdgeInsets.symmetric(
                      horizontal: width * 0.03, vertical: height * 0.01),
                  child: Row(
                    children: [
                      Container(
                        width: width * 0.1,
                        height: width * 0.1,
                        child: Icon(Icons.person),
                        decoration: BoxDecoration(color: Colors.grey),
                      ),
                      Container(
                        width: width * 0.6,
                        margin: EdgeInsets.symmetric(horizontal: width * 0.03),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Aldy $index",
                              maxLines: 1,
                              style: TextStyle(fontSize: width * 0.04),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "(Lorem ds ds d sd sd s ds d s )",
                              style: TextStyle(
                                  color: Colors.grey, fontSize: width * 0.04),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Text("2h ago")
                    ],
                  ),
                );
              }),
        ),
        Container(
          height: width * 0.1,
          padding: EdgeInsets.symmetric(horizontal: width * 0.02),
          decoration: BoxDecoration(color: Color(0xff828282)),
          child: Row(
            children: [
              Icon(Icons.insert_emoticon),
              Container(
                margin: EdgeInsets.symmetric(horizontal: width * 0.03),
                width: width * 0.5,
                child: TextFormField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: "Say something .. ds ds ds ds ds dss.",
                  ),
                ),
              ),
              Spacer(),
              Icon(
                Icons.arrow_forward,
                color: Colors.white,
              )
            ],
          ),
        )
      ]),
    ));
  }
}
