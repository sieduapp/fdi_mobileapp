import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:fdi/components/cons.dart';
import 'package:fdi/screens/game_detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class GameScreen extends StatefulWidget {
  GameScreen({Key key}) : super(key: key);

  @override
  _GameScreenState createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  Future<String> homeGames() async {
    var response =
        await http.get(Uri.encodeFull(domain + "api/gethomegames?limit=10"));
    return response.body;
  }

  @override
  Widget build(BuildContext context) {
    final TextEditingController _search = TextEditingController();
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: const Color(0xff284356),
          ),
          backgroundColor: const Color(0xffeeeeee),
          elevation: 0.0,
          title: Text(
            "Games",
            style: TextStyle(
                color: Colors.red, fontWeight: FontWeight.bold, fontSize: 30),
          ),
          actions: <Widget>[
            Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container(
                  height: height * 40,
                  width: width * 0.45,
                  child: TextFormField(
                    controller: _search,
                    decoration: InputDecoration(
                      hintText: "Search something..",
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.red,
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      contentPadding: const EdgeInsets.only(
                        top: 0.00,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    cursorColor: Colors.black,
                    style: TextStyle(color: Colors.black),
                  ),
                )),
          ],
        ),
        body: SingleChildScrollView(
            child: Column(children: [
          SizedBox(
            height: width * 0.03,
          ),
          Container(
            height: width * 0.55,
            child: FutureBuilder<dynamic>(
              future: homeGames(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState != ConnectionState.done)
                  return Center(child: CircularProgressIndicator());
                //load null data UI
                if (!snapshot.hasData || snapshot.data == null)
                  return Container();
                //load empty data UI
                if (snapshot.data.isEmpty) return Container();
                var data = json.decode(snapshot.data);
                var gameBanner = data['data'];
                print(gameBanner);
//                      return Text("Bismillah");
                return CarouselSlider.builder(
                    options: CarouselOptions(
                      autoPlay: true,
                      aspectRatio: 2.0,
                      enlargeCenterPage: false,
                      viewportFraction: 1,
                      height: width * 0.5,
                    ),
                    itemCount: gameBanner.length,
                    itemBuilder: (context, idx) {
                      return Stack(
                        children: [
                          Container(
                            width: width,
                            height: width * 0.5,
                            margin:
                                EdgeInsets.symmetric(horizontal: width * 0.01),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage(
                                      gameBanner[idx]['game_thumbnail']),
                                  fit: BoxFit.fitWidth,
                                  alignment: Alignment.topCenter,
                                ),
                                borderRadius:
                                    BorderRadius.circular(width * 0.04)),
//                                child: Text("YOUR TEXT"),
                          ),
                          Positioned(
                            bottom: width * 0.03,
                            left: width * 0.03,
                            child: Column(
                              children: [
                                Text(
                                  gameBanner[idx]['game_name'],
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: width * 0.05),
                                ),
                                Container(
                                  decoration: BoxDecoration(color: Colors.grey),
                                  child: Text(
                                    gameBanner[idx]['game_category_name'],
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: width * 0.04),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width * 0.02,
                                      vertical: width * 0.003),
                                ),
                              ],
                              crossAxisAlignment: CrossAxisAlignment.start,
                            ),
                          )
                        ],
                      );
                    });
              },
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: width * 0.03, vertical: width * 0.03),
            decoration: BoxDecoration(color: Color(0xffF5F2F2)),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: width * 0.03, vertical: width * 0.03),
//                        color: Colors.white,
                  child: Row(
                    children: [
                      Container(
                        child: Image.asset('lib/images/game_hot.png'),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: width * 0.03),
                        child: Text(
                          "Game Populer",
                          style: TextStyle(fontSize: width * 0.06),
                        ),
                      ),
                      Spacer(),
                      Container(
                        child: Text(
                          "",
                          style: TextStyle(
                              fontSize: width * 0.04, color: Colors.blue),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
//                        height: width * 3,
                  color: Colors.white,
                  child: FutureBuilder<dynamic>(
                    future: homeGames(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState != ConnectionState.done)
                        return Center(child: CircularProgressIndicator());
                      //load null data UI
                      if (!snapshot.hasData || snapshot.data == null)
                        return Container();
                      //load empty data UI
                      if (snapshot.data.isEmpty) return Container();
                      var data = json.decode(snapshot.data);
                      var gameBanner = data['data'];
                      print(gameBanner);
//                      return Text("Bismillah");
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemCount: gameBanner.length,
                        itemBuilder: (context, idx) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (_) {
                                return GameDetailScreen(
                                    gameId: gameBanner[idx]["game_id"],
                                    gameName: gameBanner[idx]["game_name"]);
                              }));
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: width * 0.03,
                                  vertical: width * 0.01),
                              margin: EdgeInsets.only(bottom: width * 0.03),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 0.50, color: Colors.grey)),
                              child: Row(
                                children: [
                                  Container(
                                    width: width * 0.15,
                                    height: width * 0.15,
                                    decoration: BoxDecoration(
//                                    borderRadius: BorderRadius.circular(10.0),
                                        ),
                                    child: ClipRRect(
                                      borderRadius:
                                          BorderRadius.circular(width * 0.03),
                                      child: Image.network(
                                          gameBanner[idx]['game_thumbnail'],
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  Container(
                                    width: width * 0.45,
                                    margin: EdgeInsets.symmetric(
                                        horizontal: width * 0.03),
                                    child: Text(
                                      gameBanner[idx]['game_name'],
                                      maxLines: 2,
                                      style: TextStyle(fontSize: width * 0.04),
                                    ),
                                  ),
                                  Spacer(),
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width * 0.02,
                                        vertical: width * 0.005),
                                    color: Colors.grey,
                                    child: Text(
                                      gameBanner[idx]['game_category_name'],
                                      style: TextStyle(
                                          fontSize: width * 0.03,
                                          color: Colors.white),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: width * 0.04),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Game",
                  style: TextStyle(fontSize: width * 0.07),
                ),
                Text(
                  "",
                  style: TextStyle(color: Colors.blue),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: width * 0.03, top: width * 0.05),
            height: width * 0.5,
            child: FutureBuilder<dynamic>(
              future: homeGames(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState != ConnectionState.done)
                  return Center(child: CircularProgressIndicator());
                //load null data UI
                if (!snapshot.hasData || snapshot.data == null)
                  return Container();
                //load empty data UI
                if (snapshot.data.isEmpty) return Container();
                var data = json.decode(snapshot.data);
                var gameBanner = data['data'];
                print(gameBanner);
//                      return Text("Bismillah");
                return ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  physics: ClampingScrollPhysics(),
                  itemCount: gameBanner.length,
                  itemBuilder: (context, idx) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return GameDetailScreen(
                            gameId: gameBanner[idx]["game_id"],
                            gameName: gameBanner[idx]["game_name"],
                          );
                        }));
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: width * 0.03),
                        width: width * 0.4,
//                            decoration: BoxDecoration(
//                                border: Border.all(color: Colors.grey)
//                            ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(width * 0.02),
                              child: Image.network(
                                  gameBanner[idx]['game_thumbnail'],
                                  fit: BoxFit.cover),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: width * 0.02),
                              child: Text(
                                gameBanner[idx]['game_name'],
                                style: TextStyle(fontSize: width * 0.03),
                                maxLines: 1,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: width * 0.02),
                              padding: EdgeInsets.symmetric(
                                  horizontal: width * 0.02,
                                  vertical: width * 0.005),
                              color: Colors.grey,
                              child: Text(
                                gameBanner[idx]['game_category_name'],
                                style: TextStyle(
                                    fontSize: width * 0.03,
                                    color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ])));
  }
}
