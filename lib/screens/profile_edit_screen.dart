import 'package:fdi/blocs/profile_bloc.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:flutter/material.dart';

class ProfileEditScreen extends StatefulWidget {
  ProfileEditScreen({Key key}) : super(key: key);

  @override
  _ProfileEditScreenState createState() => _ProfileEditScreenState();
}

class _ProfileEditScreenState extends State<ProfileEditScreen> {
  TextEditingController useridController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController nohpController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    profileBloc..getUser();
    getValue();
    super.initState();
  }

  Future<void> editUser() async {
    _onLoading();
    profileBloc
        .editUser(
            useridController.text,
            nameController.text,
            usernameController.text,
            emailController.text,
            nohpController.text,
            passwordController.text)
        .then((result) => {
              if (result) {onProcess()} else {Navigator.pop(context)}
            });
  }

  void onProcess() {
    profileBloc..getUser();
    Navigator.pop(context, true);
  }

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            height: MediaQuery.of(context).size.height * 0.1,
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: new CircularProgressIndicator(
                    backgroundColor: Colors.white,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                new Text("Loading..."),
              ],
            ),
          ),
        );
      },
    );
    new Future.delayed(new Duration(seconds: 3), () {
      Navigator.pop(context); //pop dialog
      // _login();
    });
  }

  void getValue() {
    profileBloc.subjectUser.stream.listen((data) {
      bindData(data);
    });
  }

  void bindData(dynamic data) {
    useridController.text = data["user_id"];
    nameController.text = data["name"];
    usernameController.text = data["user_name"];
    emailController.text = data["email"];
    nohpController.text = data["phone"];
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
//        iconTheme: IconThemeData(
//            color: Colors.black
//        ),
          title: Text("Edit Profile"),
//        backgroundColor: Colors.white,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [_dataEdit(context)],
          ),
        ));
  }

  Widget _dataEdit(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder(
        stream: profileBloc.subjectUser.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;

            return Form(
              key: _formKey,
              child: Container(
                margin: EdgeInsets.symmetric(
                    horizontal: width * 0.1, vertical: width * 0.1),
                padding: EdgeInsets.all(width * 0.05),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(width * 0.05),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 1,
//                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Visibility(
                      visible: false,
                      child: CurrentTextForm(
                        label: "User",
                        hintText: data["name"] ?? "",
                        controller: useridController,
                      ),
                    ),
                    CurrentTextForm(
                      label: "Nama lengkap",
                      hintText: data["name"] ?? "",
                      controller: nameController,
                    ),
                    CurrentTextForm(
                      label: "Username",
                      hintText: data["user_name"] ?? "",
                      controller: usernameController,
                    ),
                    CurrentTextForm(
                      label: "Email",
                      hintText: data["email"] ?? "",
                      controller: emailController,
                    ),
                    CurrentTextForm(
                      label: "No. Handphone",
                      hintText: data["phone"] ?? "",
                      controller: nohpController,
                    ),
                    CurrentTextForm(
                      label: "Password  ",
                      hintText: "",
                      controller: passwordController,
                    ),
                    CurrentTextForm(
                      label: "Confirm Password",
                      hintText: "",
                      controller: confirmPasswordController,
                    ),
                    InkWell(
                      onTap: () => editUser(),
                      child: Container(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: width * 0.01, horizontal: width * 0.05),
                          decoration: BoxDecoration(
                            color: Colors.orange,
                            borderRadius: BorderRadius.circular(width * 0.03),
                          ),
                          child: Text(
                            "SAVE",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          }

          return buildLoadingWidget();
        });
  }
}

class CurrentTextForm extends StatelessWidget {
  final String label;
  final String hintText;
  final TextEditingController controller;

  CurrentTextForm({this.label, this.hintText, this.controller});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
//            padding: EdgeInsets.symmetric(vertical: width * 0.03),
            alignment: Alignment.topLeft,
            child: Text(
              this.label,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: width * 0.05),
            height: width * 0.1,
            decoration: BoxDecoration(
                color: Color(0xffF5F2F2),
                borderRadius: BorderRadius.circular(width * 0.05)),
            child: TextFormField(
              controller: controller,
              decoration: InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                hintText: this.hintText,
              ),
            ),
          ),
          SizedBox(
            height: width * 0.03,
          )
        ],
      ),
    );
  }
}
