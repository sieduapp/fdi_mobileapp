import 'package:fdi/blocs/profile_bloc.dart';
import 'package:fdi/components/cons.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:fdi/screens/profile_edit_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../restart_widget.dart';
import 'shared/responsive_widget.dart';

enum menus { logout }

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  void initState() {
    // TODO: implement initState
    profileBloc..getUser();
    profileBloc..getReferral();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
//      appBar: AppBar(
//        iconTheme: IconThemeData(
//            color: Colors.black
//        ),
//        title: Text("News",style: TextStyle(color: Colors.black),),
//        backgroundColor: Colors.white,
//      ),
        body: SingleChildScrollView(
      child: Column(
        children: [
          Stack(
            children: <Widget>[
              Container(
//                margin: EdgeInsets.only(top: width * 0.03),
                padding: EdgeInsets.all(width * 0.07),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(width * 0.03),
                        bottomRight: Radius.circular(width * 0.03))),
                child: _profileheader(context),
              ),

              Positioned(
                right: 10,
                top: 10,
                child: myPopMenu()
              ),

            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: width * 0.1, vertical: width * 0.05),
            padding: EdgeInsets.all(width * 0.05),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(width * 0.05),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 1,
//                      offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: _profilePersonal(context),
          ),
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: width * 0.1, vertical: width * 0.05),
            padding: EdgeInsets.all(width * 0.05),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(width * 0.05),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 1,
//                      offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: Column(
//                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    children: [
                      Text("Member List"),
                      Spacer(),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: width * 0.01),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(width * 0.05),
                          color: Color(0xffF5F2F2),
                        ),
                        child: Row(
                          children: [
                            Icon(Icons.search),
                            Container(
                              width: width * 0.35,
                              height: width * 0.1,
                              child: TextField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Search'),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.black),
                Container(
                  child: Column(
                    children: [
                      _profileReferral(context),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //   children: [
                      //     Text("1. Rachel Kinanti"),
                      //     Text(
                      //       "Gold",
                      //       style: TextStyle(color: Colors.grey),
                      //     )
                      //   ],
                      // ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //   children: [
                      //     Text("1. Rachel Kinanti"),
                      //     Text(
                      //       "Gold",
                      //       style: TextStyle(color: Colors.grey),
                      //     )
                      //   ],
                      // ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //   children: [
                      //     Text("1. Rachel Kinanti"),
                      //     Text(
                      //       "Gold",
                      //       style: TextStyle(color: Colors.grey),
                      //     )
                      //   ],
                      // )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ));

  }

  Widget _profileReferral(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      child: StreamBuilder<dynamic>(
          stream: profileBloc.subjectReferral.stream,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data;
              return ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    return Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("${index + 1}. " + data[index]['name']),
                          Text(
                            data[index]['level_name'],
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      ),
                    );
                  });
            } else if (!snapshot.hasData) {
              return buildLoadingWidget();
            }
            return Container();
          }),
    );
  }

  Widget _profilePersonal(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      child: StreamBuilder<dynamic>(
          stream: profileBloc.subjectUser.stream,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data;
              return Column(
                children: [
                  Container(
                    alignment: Alignment.topRight,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return ResponsiveWidget(
                            builder: (context, constraints) {
                              return ProfileEditScreen();
                            },
                          );
                        }));
                      },
                      child: Text(
                        "Edit",
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                  ),
                  CurrentTextForm(
                    label: "Nama lengkap",
                    hintText: data["name"] ?? "",
                  ),
                  CurrentTextForm(
                    label: "Username",
                    hintText: data["user_name"] ?? "",
                  ),
                  CurrentTextForm(
                    label: "Email",
                    hintText: data["email"] ?? "",
                  ),
                  CurrentTextForm(
                    label: "No. Handphone",
                    hintText: data["phone"] ?? "",
                  )
                ],
              );
            }

            return buildLoadingWidget();
          }),
    );
  }

  Widget _profileheader(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder<dynamic>(
        stream: profileBloc.subjectUser.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            var imageUser = data['img'] == null
                ? domain + "assets/img/nopicture.jpg"
                : domain + "assets/img/" + data['img'];
            return Column(
              children: [
                Container(
                  child: Text(
                    "Profile",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: width * 0.06,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: width * 0.03),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(width * 0.2),
                          child: Image.network(imageUser, fit: BoxFit.cover),
                        ),
                        width: width * 0.2,
                        height: width * 0.2,
                      ),
                      SizedBox(
                        width: width * 0.03,
                      ),
                      Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: width * 0.01),
                            child: Text(
                              data["name"],
                              maxLines: 1,
                              style: TextStyle(
                                  color: Colors.white, fontSize: width * 0.05),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.circular(width * 0.03)),
                            padding: EdgeInsets.symmetric(
                                horizontal: width * 0.03,
                                vertical: width * 0.01),
                            child: Row(
                              children: [
                                Image.asset(
                                  'lib/images/icon_silver.png',
                                  fit: BoxFit.cover,
                                ),
                                SizedBox(width: 5),
                                Text(
                                  data["level_name"],
                                  style: TextStyle(color: Colors.grey),
                                )
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Column(
                          children: [
                            Text(
                              data["total_member"] ?? "",
                              style: TextStyle(
                                  color: Colors.white, fontSize: width * 0.05),
                            ),
                            Text(
                              "Total Member",
                              style: TextStyle(
                                  color: Colors.white, fontSize: width * 0.04),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          margin:
                              EdgeInsets.symmetric(horizontal: width * 0.03),
                          child: Text(
                            "|",
                            style: TextStyle(
                                color: Colors.white, fontSize: width * 0.1),
                          )),
                      Container(
                        child: Column(
                          children: [
                            Text(
                              data["poin_value"] ?? "",
                              style: TextStyle(
                                  color: Colors.white, fontSize: width * 0.05),
                            ),
                            Text(
                              "Poin Anda",
                              style: TextStyle(
                                  color: Colors.white, fontSize: width * 0.04),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            );
          }

          return buildLoadingWidget();
        });
  }

  Widget myPopMenu() {
    return PopupMenuButton(
        color: Colors.white,
        child: Icon(
          Icons.more_vert,
          color: Colors.white,
        ),
        onSelected: (value) async{
          if(value == 1){
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.clear();
            RestartWidget.of(context).restartApp();
          }
        },
        itemBuilder: (context) => [
          PopupMenuItem(
              value: 1,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.logout),
                          onPressed: () async {

                          },
                        ),
                        Text(
                          "Logout"
                        ),
                      ],
                    ),

                  ),

                ],
              )),
        ]);
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

class CurrentTextForm extends StatelessWidget {
  final String label;
  final String hintText;

  CurrentTextForm({
    this.label,
    this.hintText,
  });

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
//            padding: EdgeInsets.symmetric(vertical: width * 0.03),
            alignment: Alignment.topLeft,
            child: Text(
              this.label,
            ),
          ),
          SizedBox(
            height: width * 0.02,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: width * 0.05),
            height: width * 0.1,
            decoration: BoxDecoration(
                color: Color(0xffF5F2F2),
                borderRadius: BorderRadius.circular(width * 0.05)),
            child: TextFormField(
              enabled: false,
              decoration: InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                hintText: this.hintText,
              ),
            ),
          ),
          SizedBox(
            height: width * 0.03,
          )
        ],
      ),
    );
  }
}
