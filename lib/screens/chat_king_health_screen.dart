import 'package:flutter/material.dart';

import 'chat_room_screen.dart';
import 'shared/responsive_widget.dart';

class ChatKingHealthScreen extends StatefulWidget {
  ChatKingHealthScreen({Key key}) : super(key: key);

  @override
  _ChatKingHealthScreenState createState() => _ChatKingHealthScreenState();
}

class _ChatKingHealthScreenState extends State<ChatKingHealthScreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
//      appBar: AppBar(
//        title: Text(widget.title),
//      ),
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xffF1F8F4),
      body: Container(
          margin: EdgeInsets.all(width * 0.05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    width: width * 0.55,
                    child: DropdownButtonFormField(
                      // hint: Text("Input Referral ID"),
                      value: "King Health",
                      items: ["King Health"].map((value) {
                        return DropdownMenuItem(
                          child: Text(value),
                          value: value,
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
//                            _valFriends = value;
                        });
                      },
                      isExpanded: true,
                      decoration: const InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        border: const OutlineInputBorder(),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 0.0, horizontal: 15.0),
                      ),
                      style: TextStyle(
                          color: Color.fromRGBO(41, 130, 75, 1.0),
                          fontSize: 30,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: width * 0.03),
                    child: ButtonTheme(
                      minWidth: width * 0.1,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: RaisedButton(
                        child: Image(
                            image: AssetImage('lib/images/chat_filter.png'),
                            height: width * 0.05,
                            width: width * 0.05,
                            fit: BoxFit.fitWidth),
                        color: Colors.white,
                        onPressed: () {},
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: width * 0.03),
                    child: ButtonTheme(
                      minWidth: width * 0.1,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: RaisedButton(
                        child: Image(
                            image: AssetImage('lib/images/chat_add.png'),
                            height: width * 0.05,
                            width: width * 0.05,
                            fit: BoxFit.fitWidth),
                        color: Colors.white,
                        onPressed: () {},
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: width * 0.02),
                child: Row(
                  children: [
                    Container(
                      width: width * 0.70,
                      child: TextFormField(
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: InputBorder.none,
                          hintText: "Cari pesan ...",
                        ),
                      ),
                    ),
                    ButtonTheme(
                      minWidth: width * 0.1,
                      height: width * 0.10,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10.0),
                            bottomRight: Radius.circular(10.0)),
                      ),
                      child: RaisedButton(
                        color: Color(0xffE2E1E1),
                        child: Icon(
                          Icons.search,
                          color: Colors.grey,
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: width * 0.05),
                child: Text(
                  "Dokter Umum",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: width * 0.02),
                child: Row(
                  children: [
                    Image(
                        image: AssetImage('lib/images/icon_women.png'),
                        height: width * 0.13,
                        width: width * 0.13,
                        fit: BoxFit.fitWidth),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: width * 0.03),
                      width: width * 0.45,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Rachel Kinanti",
                            style: TextStyle(fontWeight: FontWeight.bold),
                            maxLines: 1,
                          ),
                          Text(
                            "Game King Poin Seru juga yah",
                            style: TextStyle(color: Colors.grey),
                            maxLines: 1,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.star,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                              Icon(
                                Icons.star,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                              Icon(
                                Icons.star,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                              Icon(
                                Icons.star_half,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                              Icon(
                                Icons.star_border,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: width * 0.03),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(20.0)),
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: width * 0.02, horizontal: width * 0.03),
                          child: Text(
                            "CHAT NOW",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: width * 0.025),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: width * 0.05),
                child: Text(
                  "Dokter Spesialis",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: width * 0.02),
                child: Row(
                  children: [
                    Image(
                        image: AssetImage('lib/images/icon_women.png'),
                        height: width * 0.13,
                        width: width * 0.13,
                        fit: BoxFit.fitWidth),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: width * 0.03),
                      width: width * 0.45,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Rachel Kinanti",
                            style: TextStyle(fontWeight: FontWeight.bold),
                            maxLines: 1,
                          ),
                          Text(
                            "Game King Poin Seru juga yah sdadas dsad asdasdsa asdas ",
                            style: TextStyle(color: Colors.grey),
                            maxLines: 1,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.star,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                              Icon(
                                Icons.star,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                              Icon(
                                Icons.star,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                              Icon(
                                Icons.star_half,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                              Icon(
                                Icons.star_border,
                                size: width * 0.04,
                                color: Colors.grey,
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: width * 0.03),
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(20.0)),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (_) {
                                return ResponsiveWidget(
                                  builder: (context, constraints) {
                                    // return ChatRoomScreen();
                                  },
                                );
                              }));
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: width * 0.02,
                                  horizontal: width * 0.03),
                              child: Text(
                                "CHAT NOW",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: width * 0.025),
                              ),
                            ),
                          )),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
