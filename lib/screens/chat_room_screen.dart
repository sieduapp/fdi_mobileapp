import 'package:cached_network_image/cached_network_image.dart';
import 'package:fdi/controllers/firebaseController.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class ChatRoomScreen extends StatefulWidget {
  ChatRoomScreen({Key key, this.userToID, this.userToName, this.userMeID}) : super(key: key);
  final String userToID;
  final String userToName;
  final String userMeID;

  @override
  _ChatRoomScreenState createState() => _ChatRoomScreenState();
}

class _ChatRoomScreenState extends State<ChatRoomScreen> {
  final TextEditingController _msgTextController = new TextEditingController();
  final ScrollController _chatListController = ScrollController();
  String messageType = '';
  bool _isLoading = false;
  int chatListLength = 20;
  double _scrollPosition = 560;

  _scrollListener() {
    setState(() {
      if (_scrollPosition < _chatListController.position.pixels) {
        _scrollPosition = _scrollPosition + 560;
        chatListLength = chatListLength + 20;
      }
//      _scrollPosition = _chatListController.position.pixels;
      print('list view position is $_scrollPosition');
    });
  }

  @override
  void initState() {
    super.initState();
    _chatListController.addListener(_scrollListener);
    firebaseControl();
  }

  FirebaseDatabase dbRef;
  void firebaseControl() async {
    dbRef = await FirebaseController.instanace.config();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
     appBar: AppBar(
       iconTheme: IconThemeData(
         color: Colors.black, //change your color here
       ),
       title: Row(
         children: <Widget>[
           Image(
               image: AssetImage('lib/images/icon_women.png'),
               height: width * 0.10,
               width: width * 0.10,
               fit: BoxFit.fitWidth),
           SizedBox(
             width : 10,
           ),
           Text(widget.userToName, style: TextStyle(color: Color.fromRGBO(71, 71, 71, 1.0)))
         ],
       ),
       backgroundColor: Color.fromRGBO(245, 245, 245, 1.0),
       elevation: 0.0,
     ),
        resizeToAvoidBottomPadding: false,
        backgroundColor: Color(0xffF1F8F4),
        body: StreamBuilder<Event>(
            stream: dbRef == null ? null : dbRef.reference().child("messages").orderByChild("halaman").equalTo(widget.userToID).onValue,
            builder: (context, snapshot) {
            if (!snapshot.hasData) return LinearProgressIndicator();
            Map dataMap = snapshot.data != null
            ? snapshot.data.snapshot.value
                : null;
            return Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView(
                          reverse: true,
                          shrinkWrap: true,
                          padding: const EdgeInsets.fromLTRB(4.0,10,4,10),
                          controller: _chatListController,
                          children: dataMap.values.map((data) {//snapshot.data.documents.reversed.map((data) {
                            print(widget.userMeID);
                            return data['id'] == widget.userMeID
                                ? _listItemOther(
                                context,
                                widget.userToName,
                                "",
                                data['message'],
                                "text")
                                : _listItemMine(
                                context,
                                data['message'],
                                "text");
                          }).toList()
                      ),
                    ),
                    _buildTextComposer(),
                  ],
                ),
//                Positioned(
//                  // Loading view in the center.
//                  child: _isLoading
//                      ? Container(
//                    child: Center(
//                      child: CircularProgressIndicator(),
//                    ),
//                    color: Colors.white.withOpacity(0.7),
//                  )
//                      : Container(),
//                ),
              ],
            );

            }),
    //     SingleChildScrollView(
    //       child: Container(
    //         child: StreamBuilder<Event>(
    //             stream: dbRef == null ? null : dbRef.reference().child("messages").orderByChild("halaman").equalTo(widget.userToID).onValue,
    //             builder: (context, snapshot) {
    //               if (!snapshot.hasData) return LinearProgressIndicator();
    //               Map dataMap = snapshot.data != null
    //                   ? snapshot.data.snapshot.value
    //                   : null;
    //               return ListView(
    //                   reverse: true,
    //                   shrinkWrap: true,
    //                   padding:
    //                   const EdgeInsets.fromLTRB(4.0, 10, 4, 10),
    //                   controller: _chatListController,
    //                   children: dataMap.values.map((data) {
    //                     //snapshot.data.documents.reversed.map((data) {
    //                     print(data);
    //                     return data['halaman'] == widget.userToID
    //                         ? _listItemOther(
    //                         context,
    //                         widget.userToName,
    //                         "",
    //                         data['message'],
    //                         "text")
    //                         : _listItemMine(
    //                         context,
    //                         data['message'],
    //                         "text");
    //                   }).toList());
    //               //   Column(
    //               //   children: <Widget>[
    //               //     // Text("Die"),
    //               //     Expanded(
    //               //       flex: 1,
    //               //         child: Column(
    //               //           children: <Widget>[
    //               //             Text("Oke"),
    //               //           ],
    //               //         )
    //               //     )
    //               //     // Expanded(
    //               //     //   child: Text("Okee"),
    //               //     // ),
    //               //   ],
    //               // );
    //               // return  Column(
    //               //   children: <Widget>[
    //               //     Expanded(
    //               //       flex: 1,
    //               //       child:
    //               //       ListView(
    //               //           reverse: true,
    //               //           shrinkWrap: true,
    //               //           padding:
    //               //           const EdgeInsets.fromLTRB(4.0, 10, 4, 10),
    //               //           controller: _chatListController,
    //               //           children: dataMap.values.map((data) {
    //               //             //snapshot.data.documents.reversed.map((data) {
    //               //             print(data);
    //               //             return data['halaman'] == widget.userToID
    //               //                 ? _listItemOther(
    //               //                 context,
    //               //                 widget.userToName,
    //               //                 "",
    //               //                 data['message'],
    //               //                 "text")
    //               //                 : _listItemMine(
    //               //                 context,
    //               //                 data['message'],
    //               //                 "text");
    //               //           }).toList()),
    //               //     ),
    //               //     _buildTextComposer(),
    //               //   ],
    //               // );
    //               //   children: <Widget>[
    //
    // //                Positioned(
    // //                  // Loading view in the center.
    // //                  child: _isLoading
    // //                      ? Container(
    // //                    child: Center(
    // //                      child: CircularProgressIndicator(),
    // //                    ),
    // //                    color: Colors.white.withOpacity(0.7),
    // //                  )
    // //                      : Container(),
    // //                ),
    // //                 ],
    // //               );
    //             }),
    //       )
    //     )
    );
  }

  Widget _buildTextComposer() {
    return new IconTheme(
      data: new IconThemeData(color: Theme.of(context).accentColor),
      child: new Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: new Row(
          children: <Widget>[
            new Container(
              margin: new EdgeInsets.symmetric(horizontal: 2.0),
              child: new IconButton(
                  icon: new Icon(
                    Icons.photo,
                    color: Colors.cyan[900],
                  ),
                  onPressed: () {
                    // PickImageController.instance
                    //     .cropImageFromFile()
                    //     .then((croppedFile) {
                    //   if (croppedFile != null) {
                    //     setState(() {
                    //       messageType = 'image';
                    //       _isLoading = true;
                    //     });
                    //     _saveUserImageToFirebaseStorage(croppedFile);
                    //   } else {
                    //     _showDialog('Pick Image error');
                    //   }
                    // });
                  }),
            ),
            new Flexible(
              child: new TextField(
                controller: _msgTextController,
                onSubmitted: _handleSubmitted,
                decoration:
                new InputDecoration.collapsed(hintText: "Send a message"),
              ),
            ),
            new Container(
              margin: new EdgeInsets.symmetric(horizontal: 2.0),
              child: new IconButton(
                  icon: new Icon(Icons.send),
                  onPressed: () {
                    setState(() {
                      messageType = 'text';
                    });
                    _handleSubmitted(_msgTextController.text);
                  }),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _handleSubmitted(String text) async {
    try {
      setState(() {
        _isLoading = true;
      });
      // await FirebaseController.instanace.sendMessageToChatRoom(
      //     widget.myName, widget.myID, widget.selectedUserID, text);
      // await FirebaseController.instanace.updateChatRequestField(
      //     widget.selectedUserID,
      //     messageType == 'text' ? text : '(Photo)',
      //     widget.chatID,
      //     widget.myID,
      //     widget.selectedUserID);
      // await FirebaseController.instanace.updateChatRequestField(
      //     widget.myID,
      //     messageType == 'text' ? text : '(Photo)',
      //     widget.chatID,
      //     widget.myID,
      //     widget.selectedUserID);
      // _getUnreadMSGCountThenSendMessage();
    } catch (e) {
      // _showDialog('Error user information to database');
      // _resetTextFieldAndLoading();
    }
  }

  Widget _listItemOther(BuildContext context, String name, String thumbnail, String message, String type) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(top: 4.0),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(24.0),
                      child: CachedNetworkImage(
                        imageUrl: thumbnail,
                        placeholder: (context, url) => Container(
                          transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                          child: Container(
                              width: 60,
                              height: 60,
                              child: Center(
                                  child: new CircularProgressIndicator())),
                        ),
                        errorWidget: (context, url, error) =>
                        new Icon(Icons.error),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(name),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 4, 0, 8),
                          child: Container(
                            constraints:
                            BoxConstraints(maxWidth: size.width - 150),
                            decoration: BoxDecoration(
                              color: Colors.grey[200],
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Padding(
                              padding:
                              EdgeInsets.all(type == 'text' ? 10.0 : 0),
                              child: Container(
                                  child: type == 'text'
                                      ? Text(
                                    message,
                                    style: TextStyle(color: Colors.black),
                                  )
                                      : _imageMessage(message)),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 14.0, left: 4),
                          child: Text("",
                            // time,
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _imageMessage(imageUrlFromFB) {
    return Container(
      width: 160,
      height: 160,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: GestureDetector(
        onTap: () {
          // Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //         builder: (context) => FullPhoto(url: imageUrlFromFB)));
        },
        child: CachedNetworkImage(
          imageUrl: imageUrlFromFB,
          placeholder: (context, url) => Container(
            transform: Matrix4.translationValues(0, 0, 0),
            child: Container(
                width: 60,
                height: 80,
                child: Center(child: new CircularProgressIndicator())),
          ),
          errorWidget: (context, url, error) => new Icon(Icons.error),
          width: 60,
          height: 80,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _listItemMine(BuildContext context, String message, String type) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(top: 2.0, right: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 14.0, right: 2, left: 4),
            child: Text("",
              // isRead ? '' : '1',
              style: TextStyle(fontSize: 12, color: Colors.yellow[900]),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 14.0, right: 4, left: 8),
            child: Text("",
              // time,
              style: TextStyle(fontSize: 12),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 4, 8),
                child: Container(
                  constraints:
                  BoxConstraints(maxWidth: size.width - size.width * 0.26),
                  decoration: BoxDecoration(
                    color:
                    type == 'text' ? Colors.green[700] : Colors.transparent,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(type == 'text' ? 10.0 : 0),
                    child: Container(
                        child: type == 'text'
                            ? Text(
                          message,
                          style: TextStyle(color: Colors.white),
                        )
                            : _imageMessage(message)),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

// import 'dart:async';
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:fdi/controllers/firebaseController.dart';
// import 'package:fdi/controllers/utils.dart';
// import 'package:chatapploydlab/Controllers/notificationController.dart';
// import 'package:firebase_database/firebase_database.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_widgets/flutter_widgets.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'Controllers/firebaseController.dart';
// import 'Controllers/pickImageController.dart';
// import 'Controllers/utils.dart';
// import 'fullphoto.dart';

// class ChatRoomScreen extends StatefulWidget {
//   ChatRoomScreen(this.myID, this.myName, this.selectedUserID, this.selectedUserName);
//
//   String myID;
//   String myName;
//   String selectedUserID;
//   String selectedUserName;
//
//   @override
//   _ChatRoomScreenState createState() => _ChatRoomScreenState();
// }
//
// class _ChatRoomScreenState extends State<ChatRoomScreen> {
//   final TextEditingController _msgTextController = new TextEditingController();
//   final ScrollController _chatListController = ScrollController();
//   String messageType = '';
//   bool _isLoading = false;
//   int chatListLength = 20;
//   double _scrollPosition = 560;
//
//   _scrollListener() {
//     setState(() {
//       if (_scrollPosition < _chatListController.position.pixels) {
//         _scrollPosition = _scrollPosition + 560;
//         chatListLength = chatListLength + 20;
//       }
// //      _scrollPosition = _chatListController.position.pixels;
//       print('list view position is $_scrollPosition');
//     });
//   }
//
//   @override
//   void initState() {
//     // setCurrentChatRoomID(widget.chatID);
//     // FirebaseController.instanace.getUnreadMSGCount();
//     _chatListController.addListener(_scrollListener);
//     firebaseControl();
//     super.initState();
//   }
//
//   FirebaseDatabase dbRef;
//   void firebaseControl() async {
//     // dbRef = await FirebaseController.instanace.config();
//     // _messagesRef = dbRef.reference().child("messages");
//   }
//
//   @override
//   void dispose() {
//     setCurrentChatRoomID('none');
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: Text(widget.selectedUserName, style: TextStyle(
//
//           ),),
//           // centerTitle: true,
//         ),
//         resizeToAvoidBottomPadding: false,
//         backgroundColor: Color(0xffF1F8F4),
//         body:
//         Container(
//           child: StreamBuilder<Event>(
//               stream: dbRef
//                   .reference()
//                   .child("messages")
//                   .orderByChild("halaman")
//                   .equalTo(widget.selectedUserID)
//                   .onValue,
//               builder: (context, snapshot) {
//                 if (!snapshot.hasData) return LinearProgressIndicator();
//                 return Stack(
//                   children: <Widget>[
//                     Column(
//                       children: <Widget>[
//                         // Expanded(
//                         //   child: ListView(
//                         //       reverse: true,
//                         //       shrinkWrap: true,
//                         //       padding:
//                         //       const EdgeInsets.fromLTRB(4.0, 10, 4, 10),
//                         //       controller: _chatListController,
//                         //       children: snapshot.data.snapshot.value((data) {
//                         //         //snapshot.data.documents.reversed.map((data) {
//                         //         return data['sender'] == widget.selectedUserID
//                         //             ? _listItemOther(
//                         //             context,
//                         //             widget.selectedUserName,
//                         //             "",
//                         //             data['message'],
//                         //             returnTimeStamp(data['hours']),
//                         //             data['type'])
//                         //             : _listItemMine(
//                         //             context,
//                         //             data['message'],
//                         //             returnTimeStamp(data['timestamp']),
//                         //             data['isread'],
//                         //             data['type']);
//                         //       }).toList()),
//                         // ),
//                         _buildTextComposer(),
//                       ],
//                     ),
// //                Positioned(
// //                  // Loading view in the center.
// //                  child: _isLoading
// //                      ? Container(
// //                    child: Center(
// //                      child: CircularProgressIndicator(),
// //                    ),
// //                    color: Colors.white.withOpacity(0.7),
// //                  )
// //                      : Container(),
// //                ),
//                   ],
//                 );
//               }),
//         ));
//   }
//
//   Widget _listItemOther(BuildContext context, String name, String thumbnail,
//       String message, String time, String type) {
//     final size = MediaQuery.of(context).size;
//     return Padding(
//       padding: const EdgeInsets.only(top: 4.0),
//       child: Container(
//         child: Row(
//           crossAxisAlignment: CrossAxisAlignment.end,
//           children: <Widget>[
//             Row(
//               mainAxisAlignment: MainAxisAlignment.start,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: GestureDetector(
//                     child: ClipRRect(
//                       borderRadius: BorderRadius.circular(24.0),
//                       child: CachedNetworkImage(
//                         imageUrl: thumbnail,
//                         placeholder: (context, url) => Container(
//                           transform: Matrix4.translationValues(0.0, 0.0, 0.0),
//                           child: Container(
//                               width: 60,
//                               height: 60,
//                               child: Center(
//                                   child: new CircularProgressIndicator())),
//                         ),
//                         errorWidget: (context, url, error) =>
//                         new Icon(Icons.error),
//                         width: 50,
//                         height: 50,
//                         fit: BoxFit.cover,
//                       ),
//                     ),
//                   ),
//                 ),
//                 Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     Text(name),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.end,
//                       crossAxisAlignment: CrossAxisAlignment.end,
//                       children: <Widget>[
//                         Padding(
//                           padding: const EdgeInsets.fromLTRB(0, 4, 0, 8),
//                           child: Container(
//                             constraints:
//                             BoxConstraints(maxWidth: size.width - 150),
//                             decoration: BoxDecoration(
//                               color: Colors.grey[200],
//                               borderRadius: BorderRadius.circular(10.0),
//                             ),
//                             child: Padding(
//                               padding:
//                               EdgeInsets.all(type == 'text' ? 10.0 : 0),
//                               child: Container(
//                                   child: type == 'text'
//                                       ? Text(
//                                     message,
//                                     style: TextStyle(color: Colors.black),
//                                   )
//                                       : _imageMessage(message)),
//                             ),
//                           ),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.only(bottom: 14.0, left: 4),
//                           child: Text(
//                             time,
//                             style: TextStyle(fontSize: 12),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget _listItemMine(BuildContext context, String message, String time,
//       bool isRead, String type) {
//     final size = MediaQuery.of(context).size;
//     return Padding(
//       padding: const EdgeInsets.only(top: 2.0, right: 8),
//       child: Row(
//         crossAxisAlignment: CrossAxisAlignment.end,
//         mainAxisAlignment: MainAxisAlignment.end,
//         children: <Widget>[
//           Padding(
//             padding: const EdgeInsets.only(bottom: 14.0, right: 2, left: 4),
//             child: Text(
//               isRead ? '' : '1',
//               style: TextStyle(fontSize: 12, color: Colors.yellow[900]),
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(bottom: 14.0, right: 4, left: 8),
//             child: Text(
//               time,
//               style: TextStyle(fontSize: 12),
//             ),
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               Padding(
//                 padding: const EdgeInsets.fromLTRB(0, 8, 4, 8),
//                 child: Container(
//                   constraints:
//                   BoxConstraints(maxWidth: size.width - size.width * 0.26),
//                   decoration: BoxDecoration(
//                     color:
//                     type == 'text' ? Colors.green[700] : Colors.transparent,
//                     borderRadius: BorderRadius.circular(10.0),
//                   ),
//                   child: Padding(
//                     padding: EdgeInsets.all(type == 'text' ? 10.0 : 0),
//                     child: Container(
//                         child: type == 'text'
//                             ? Text(
//                           message,
//                           style: TextStyle(color: Colors.white),
//                         )
//                             : _imageMessage(message)),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget _imageMessage(imageUrlFromFB) {
//     return Container(
//       width: 160,
//       height: 160,
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(10.0),
//       ),
//       child: GestureDetector(
//         onTap: () {
//           // Navigator.push(
//           //     context,
//           //     MaterialPageRoute(
//           //         builder: (context) => FullPhoto(url: imageUrlFromFB)));
//         },
//         child: CachedNetworkImage(
//           imageUrl: imageUrlFromFB,
//           placeholder: (context, url) => Container(
//             transform: Matrix4.translationValues(0, 0, 0),
//             child: Container(
//                 width: 60,
//                 height: 80,
//                 child: Center(child: new CircularProgressIndicator())),
//           ),
//           errorWidget: (context, url, error) => new Icon(Icons.error),
//           width: 60,
//           height: 80,
//           fit: BoxFit.cover,
//         ),
//       ),
//     );
//   }
//
//   Widget _buildTextComposer() {
//     return new IconTheme(
//       data: new IconThemeData(color: Theme.of(context).accentColor),
//       child: new Container(
//         margin: const EdgeInsets.symmetric(horizontal: 8.0),
//         child: new Row(
//           children: <Widget>[
//             new Container(
//               margin: new EdgeInsets.symmetric(horizontal: 2.0),
//               child: new IconButton(
//                   icon: new Icon(
//                     Icons.photo,
//                     color: Colors.cyan[900],
//                   ),
//                   onPressed: () {
//                     // PickImageController.instance
//                     //     .cropImageFromFile()
//                     //     .then((croppedFile) {
//                     //   if (croppedFile != null) {
//                     //     setState(() {
//                     //       messageType = 'image';
//                     //       _isLoading = true;
//                     //     });
//                     //     _saveUserImageToFirebaseStorage(croppedFile);
//                     //   } else {
//                     //     _showDialog('Pick Image error');
//                     //   }
//                     // });
//                   }),
//             ),
//             new Flexible(
//               child: new TextField(
//                 controller: _msgTextController,
//                 onSubmitted: _handleSubmitted,
//                 decoration:
//                 new InputDecoration.collapsed(hintText: "Send a message"),
//               ),
//             ),
//             new Container(
//               margin: new EdgeInsets.symmetric(horizontal: 2.0),
//               child: new IconButton(
//                   icon: new Icon(Icons.send),
//                   onPressed: () {
//                     setState(() {
//                       messageType = 'text';
//                     });
//                     _handleSubmitted(_msgTextController.text);
//                   }),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   Future<void> _saveUserImageToFirebaseStorage(croppedFile) async {
//     try {
//       // String takeImageURL = await FirebaseController.instanace
//       //     .sendImageToUserInChatRoom(croppedFile, widget.chatID);
//       // _handleSubmitted(takeImageURL);
//     } catch (e) {
//       _showDialog('Error add user image to storage');
//     }
//   }
//
//   Future<void> _handleSubmitted(String text) async {
//     try {
//       setState(() {
//         _isLoading = true;
//       });
//       await FirebaseController.instanace.sendMessageToChatRoom(
//           widget.myName, widget.myID, widget.selectedUserID, text);
//       // await FirebaseController.instanace.updateChatRequestField(
//       //     widget.selectedUserID,
//       //     messageType == 'text' ? text : '(Photo)',
//       //     widget.chatID,
//       //     widget.myID,
//       //     widget.selectedUserID);
//       // await FirebaseController.instanace.updateChatRequestField(
//       //     widget.myID,
//       //     messageType == 'text' ? text : '(Photo)',
//       //     widget.chatID,
//       //     widget.myID,
//       //     widget.selectedUserID);
//       // _getUnreadMSGCountThenSendMessage();
//     } catch (e) {
//       _showDialog('Error user information to database');
//       _resetTextFieldAndLoading();
//     }
//   }
//
//   // Future<void> _getUnreadMSGCountThenSendMessage() async {
//   //   try {
//   //     int unReadMSGCount = await FirebaseController.instanace
//   //         .getUnreadMSGCount(widget.selectedUserID);
//   //     await NotificationController.instance.sendNotificationMessageToPeerUser(
//   //         unReadMSGCount,
//   //         messageType,
//   //         _msgTextController.text,
//   //         widget.myName,
//   //         widget.chatID,
//   //         widget.selectedUserToken);
//   //   } catch (e) {
//   //     print(e.message);
//   //   }
//   //   _resetTextFieldAndLoading();
//   // }
//
//   _resetTextFieldAndLoading() {
//     FocusScope.of(context).requestFocus(FocusNode());
//     _msgTextController.text = '';
//     setState(() {
//       _isLoading = false;
//     });
//   }
//
//   _showDialog(String msg) {
//     showDialog(
//         context: context,
//         builder: (context) {
//           return AlertDialog(
//             content: Text(msg),
//           );
//         });
//   }
// }

