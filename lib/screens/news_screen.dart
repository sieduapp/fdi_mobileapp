import 'package:fdi/blocs/home_bloc.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:fdi/screens/news_detail_screen.dart';
import 'package:flutter/material.dart';
import 'shared/responsive_widget.dart';

class NewsScreen extends StatefulWidget {
  NewsScreen({Key key}) : super(key: key);

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  final TextEditingController _search = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    homeBloc..getHomeNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: const Color(0xff284356),
          ),
          backgroundColor: const Color(0xffeeeeee),
          elevation: 0.0,
          title: Text(
            "News",
            style: TextStyle(
                color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 30),
          ),
          actions: <Widget>[
            Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container(
                  height: height * 40,
                  width: width * 0.45,
                  child: TextFormField(
                    controller: _search,
                    decoration: InputDecoration(
                      hintText: "Search something..",
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.red,
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      contentPadding: const EdgeInsets.only(
                        top: 0.00,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    cursorColor: Colors.black,
                    style: TextStyle(color: Colors.black),
                  ),
                )),
          ],
        ),
        body: ListView(children: [
          Column(children: [
            SizedBox(
              height: 20,
            ),
            _contentHeader(),
            SizedBox(
              height: width * 0.03,
            ),
            Container(
              alignment: Alignment.topLeft,
              margin:
                  EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 0.1),
              child: Text(
                "News Feed",
                style: TextStyle(fontSize: width * 0.07),
              ),
            ),
            SizedBox(
              height: width * 0.03,
            ),
            contentNews(),
            SizedBox(
              height: width * 0.03,
            ),
            Container(
              alignment: Alignment.topLeft,
              margin:
                  EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 0.1),
              child: Text(
                "Berita Terpopuler",
                style: TextStyle(fontSize: width * 0.05, color: Colors.red),
              ),
            ),
            SizedBox(
              height: width * 0.03,
            ),
            _contentPopuler()
          ]),
        ]));
  }

  Widget _contentHeader() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder<dynamic>(
        stream: homeBloc.homeNews.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            return Container(
//                  color: Colors.red,
              margin: EdgeInsets.symmetric(horizontal: width * 0.03),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(width * 0.05),
                  color: Colors.red),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(width * 0.05),
                    color: Colors.red),
                // padding: EdgeInsets.symmetric(
                //     horizontal: width * 0.05, vertical: width * 0.03),
                child: InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                      return ResponsiveWidget(
                        builder: (context, constraints) {
                          return NewsDetailScreen(
                            newsid: data[0]['id'],
                          );
                        },
                      );
                    }));
                  },
                  child: Column(
                    children: [
                      Image.network(
                        data[0]['attach'],
                        width: width * 1.0,
                        height: height * 0.4,
                        fit: BoxFit.fill,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: width * 0.05,
                                vertical: width * 0.01),
                            child: Text(
                              data[0]["content1"],
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white, fontSize: width * 0.05),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: width * 0.05,
                                vertical: height * 0.01),
                            child: Text(
                              "18 menit yang lalu",
                              style: TextStyle(
                                  color: Colors.white, fontSize: width * 0.03),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          } else {
            return buildLoadingWidget();
          }
        });
  }

  Widget contentNews() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Container(
          child: StreamBuilder<dynamic>(
              stream: homeBloc.homeNews.stream,
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  var data = snapshot.data;
                  return ListView.builder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      itemCount: data.length,
                      itemBuilder: (context, index) {
                        Color color = Color(0xffF5F2F2);
                        if (index % 2 == 0) {
                          color = Color(0xffFFFFFF);
                        }
                        return InkWell(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (_) {
                              return ResponsiveWidget(
                                builder: (context, constraints) {
                                  return NewsDetailScreen(
                                    newsid: data[index]['id'],
                                  );
                                },
                              );
                            }));
                          },
                          child: ColoredBox(
                            color: color,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: width * 0.03,
                                  vertical: width * 0.01),
                              child: Row(
                                children: [
                                  ClipRRect(
                                      borderRadius: BorderRadius.circular(10.0),
                                      child: Image.network(
                                        data[index]['attach'],
                                        width: width * 0.25,
                                        height: width * 0.25,
                                        fit: BoxFit.fill,
                                      )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Flexible(
                                      child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width * 0.01),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          data[index]["content1"],
                                          maxLines: 2,
                                        ),
                                        Text(
                                          "1 jam lalu",
                                          style: TextStyle(color: Colors.grey),
                                        )
                                      ],
                                    ),
                                  )),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                } else {
                  return buildLoadingWidget();
                }
              }),
        ),
      ],
    );
  }

  Widget _contentPopuler() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder<dynamic>(
        stream: homeBloc.homeNews.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            return Container(
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return ResponsiveWidget(
                            builder: (context, constraints) {
                              return NewsDetailScreen();
                            },
                          );
                        }));
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: width * 0.03, vertical: 0.03),
                        margin: EdgeInsets.only(bottom: width * 0.03),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "#${index + 1}",
                              style: TextStyle(
                                  fontSize: width * 0.05, color: Colors.grey),
                            ),
                            Container(
                              width: width * 0.7,
                              margin: EdgeInsets.symmetric(
                                  horizontal: width * 0.05),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(data[index]["content1"],
                                      maxLines: 1,
                                      style: TextStyle(fontSize: width * 0.05)),
                                  Text(
                                    "1 jam yang lalu",
                                    style: TextStyle(color: Colors.grey),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            );
          } else {
            return buildLoadingWidget();
          }
        });
  }
}
