import 'package:fdi/blocs/home_bloc.dart';
import 'package:fdi/blocs/news_bloc.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:flutter/material.dart';

class NewsDetailScreen extends StatefulWidget {
  NewsDetailScreen({Key key, this.newsid}) : super(key: key);
  final String newsid;

  @override
  _NewsDetailScreenState createState() => _NewsDetailScreenState();
}

class _NewsDetailScreenState extends State<NewsDetailScreen> {
  @override
  void initState() {
    super.initState();
    newsBloc..getNewDetail(widget.newsid);
    homeBloc..getHomeNews();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            "News",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
        body: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            StreamBuilder(
              stream: newsBloc.newStream.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var data = snapshot.data;
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: width,
                          height: width * 0.6,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                          ),
                          margin:
                              EdgeInsets.symmetric(horizontal: width * 0.01),
                          child: Image.network(
                            data['attach'],
                            fit: BoxFit.fill,
                          )),
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: width * 0.03, vertical: width * 0.05),
                        child: Text(
                          data["name"],
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: width * 0.07),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: width * 0.03, vertical: width * 0.03),
                        child: Text(
                          data["content1"],
                          style: TextStyle(
                              fontSize: width * 0.05, color: Colors.grey),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: width * 0.03, vertical: width * 0.01),
                        child: Text(
                          "Berita terkait:",
                          style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: width * 0.05),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: width * 0.03, vertical: width * 0.03),
                        child: Text(
                          data["content2"],
                          style: TextStyle(
                              fontSize: width * 0.05, color: Colors.grey),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                        ),
                      ),
                    ],
                  );
                }

                return buildLoadingWidget();
              },
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: width * 0.03, vertical: width * 0.05),
              child: Text(
                "Berita Terkait",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: width * 0.07),
              ),
            ),
            StreamBuilder<dynamic>(
              stream: homeBloc.homeNews.stream,
              builder: (context, snapshot) {},
            ),
            Container(
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return NewsDetailScreen();
                        }));
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: width * 0.03, vertical: 0.03),
                        margin: EdgeInsets.only(bottom: width * 0.03),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: width * 0.02),
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.circular(width * 0.03),
                                child: Image.asset(
                                  'lib/images/image_video.png',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: width * 0.03),
                              child: Text(
                                "Berita terkait: IHSG siap menguat 7 hari, bakal happy cuan nih",
                                maxLines: 2,
                                style: TextStyle(fontSize: width * 0.05),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.symmetric(
                                  horizontal: width * 0.03),
                              child: Text(
                                "1 jam lalu",
                                maxLines: 2,
                                style: TextStyle(
                                    color: Colors.grey, fontSize: width * 0.05),
                              ),
                            ),
                            SizedBox(
                              height: width * 0.05,
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: width * 0.03, vertical: width * 0.05),
              child: Text(
                "Berita Terpopuler",
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: width * 0.07),
              ),
            ),
            Container(
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) {
                          return NewsDetailScreen();
                        }));
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: width * 0.03, vertical: 0.07),
                        margin: EdgeInsets.only(bottom: width * 0.05),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "#1",
                              style: TextStyle(
                                  fontSize: width * 0.05, color: Colors.grey),
                            ),
                            Container(
                              width: width * 0.7,
                              margin: EdgeInsets.symmetric(
                                  horizontal: width * 0.05),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Rekomendasi Saham dan sasa sa s as ",
                                      maxLines: 2,
                                      style: TextStyle(fontSize: width * 0.05)),
                                  Text(
                                    "1 jam yang lalu",
                                    style: TextStyle(color: Colors.grey),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          ]),
        ));
  }

  // Widget _news(){
  //   return
  // }

}
