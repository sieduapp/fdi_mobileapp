import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:fdi/blocs/home_bloc.dart';
import 'package:fdi/components/comp.dart';
import 'package:fdi/components/cons.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:fdi/screens/about_screen.dart';
import 'package:fdi/screens/game_screen.dart';
import 'package:fdi/screens/meeting_room_screnn.dart';
import 'package:fdi/screens/meeting_screen.dart';
import 'package:fdi/screens/news_screen.dart';
import 'package:fdi/screens/profile_screen.dart';
import 'package:fdi/screens/video_screen.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import 'shared/responsive_widget.dart';
import 'term_condition_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController _search = TextEditingController();

  @override
  void initState() {
    homeBloc..getIdentityVideo();
    homeBloc..getHomeGames();
    homeBloc..getHomeMeeting();
    homeBloc..getHomeNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: const Color(0xffeeeeee),
          elevation: 0.0,
          title: GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) {
                return ResponsiveWidget(
                  builder: (context, constraints) {
                    return ProfileScreen();
                  },
                );
              }));
            },
            child: Image(
              image: AssetImage('lib/images/logo-panjang.png'),
              width: width * 0.35,
            ),
          ),
          actions: <Widget>[
            Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container(
                  height: height * 40,
                  width: width * 0.45,
                  child: TextFormField(
                    controller: _search,
                    decoration: InputDecoration(
                      hintText: "Search something..",
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.red,
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      contentPadding: const EdgeInsets.only(
                        top: 0.00,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    cursorColor: Colors.black,
                    style: TextStyle(color: Colors.black),
                  ),
                )),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              contentMenu(),
              contentVideo(),
              contentGames(),
              contentMeeting(),
              contentNews()
            ],
          ),
        ),
      ),
    );
  }

  Widget contentMenu() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        Container(
          height: height * 0.5,
          width: width * 1.0,
        ),
        Padding(
          padding: EdgeInsets.only(
              top: height * 0.02, left: width * 0.10, right: width * 0.10),
          child: ColoredBox(
            color: Color(0xffF6FAFF),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(width * 0.05),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyMenuImage(
                        image: 'lib/images/menu_video.png',
                        label: 'Video',
                        navigator: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) {
                            return ResponsiveWidget(
                              builder: (context, constraints) {
                                return VideoScreen();
                              },
                            );
                          }));
                        },
                      ),
                      MyMenuImage(
                        image: 'lib/images/menu_games.png',
                        label: 'Games',
                        navigator: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) {
                            return ResponsiveWidget(
                                builder: (context, constraints) {
                              return GameScreen();
                            });
                          }));
                        },
                      ),
                      MyMenuImage(
                        image: 'lib/images/menu_room.png',
                        label: 'Meeting Room',
                        navigator: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) {
                            return ResponsiveWidget(
                              builder: (context, constraints) {
                                return MeetingScreen();
                              },
                            );
                          }));
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(width * 0.05),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyMenuImage(
                        image: 'lib/images/menu_news.png',
                        label: 'News',
                        navigator: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) {
                            return ResponsiveWidget(
                                builder: (context, constraints) {
                              return NewsScreen();
                            });
                          }));
                        },
                      ),
                      MyMenuImage(
                        image: 'lib/images/menu_about.png',
                        label: 'About Us',
                        navigator: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) {
                            return ResponsiveWidget(
                                builder: (context, constraints) {
                              return AboutScreen();
                            });
                          }));
                        },
                      ),
                      MyMenuImage(
                        image: 'lib/images/menu_term.png',
                        label: 'Terms & \n Conditions',
                        navigator: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) {
                            return ResponsiveWidget(
                                builder: (context, constraints) {
                              return TermConditionScreen();
                            });
                          }));
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 5,
          child: Image(
            image: AssetImage('lib/images/man_red.png'),
            width: width * 0.35,
          ),
        ),
        Positioned(
          bottom: 20,
          right: 0,
          child: Image(
            image: AssetImage('lib/images/man_blue.png'),
            width: width * 0.35,
          ),
        ),
      ],
    );
  }

  Widget contentVideo() {
    return StreamBuilder<String>(
        stream: homeBloc.identityVideo.stream,
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            return YoutubePlayer(
              controller: YoutubePlayerController(
                initialVideoId: data,
                flags: YoutubePlayerFlags(
                  autoPlay: false,
                ),
              ),
            );
          } else {
            return buildLoadingWidget();
          }
        });
  }

  Widget contentGames() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Games"),
          trailing: GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) {
                return ResponsiveWidget(builder: (context, constraints) {
                  return GameScreen();
                });
              }));
            },
            child: Text(
              "See more...",
              style: TextStyle(color: Color(0xff4684DB)),
            ),
          ),
        ),
        Container(
            color: Color(0xff566CC2),
            child: StreamBuilder<dynamic>(
                stream: homeBloc.homeGames.stream,
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (!snapshot.hasData || snapshot.data == null)
                    return Container();

                  if (snapshot.data.isEmpty) return Container();

                  if (snapshot.hasData && snapshot.data != null) {
                    var gameList = snapshot.data;
                    return CarouselSlider.builder(
                      options: CarouselOptions(
                        autoPlay: true,
                        aspectRatio: 2.0,
                        enlargeCenterPage: false,
                        viewportFraction: 1,
                        height: width * 0.45,
                      ),
                      itemCount: (gameList.length / 2).round(),
                      itemBuilder: (context, index) {
                        final int first = index * 2;
                        final int second = first + 1;
                        return Row(
                          children: [first, second].map((idx) {
                            return Expanded(
                              flex: 1,
                              child: Container(
                                margin: EdgeInsets.all(width * 0.02),
                                child: Card(
                                  child: Column(
                                    children: [
                                      Image.network(
                                          gameList[idx]['game_thumbnail'],
                                          fit: BoxFit.cover),
                                      Container(
                                          padding: EdgeInsets.all(width * 0.01),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                child: Text(
                                                  gameList[idx]['game_name'],
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      fontSize: width * 0.03),
                                                ),
                                                width: width * 0.25,
                                              ),
                                              Spacer(),
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: width * 0.02,
                                                    vertical: width * 0.01),
                                                decoration: BoxDecoration(
                                                  color: Colors.orange,
                                                ),
                                                child: Text(
                                                  'PLAY NOW',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: width * 0.025),
                                                ),
                                              )
                                            ],
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }).toList(),
                        );
                      },
                    );
                  }
                  return Container();
                })),
      ],
    );
  }

  Widget contentMeeting() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Meeting Room"),
          trailing: GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) {
                return ResponsiveWidget(builder: (context, constraints) {
                  return MeetingScreen();
                });
              }));
            },
            child: Text(
              "See more...",
              style: TextStyle(color: Color(0xff4684DB)),
            ),
          ),
        ),
        StreamBuilder<dynamic>(
            stream: homeBloc.homeMeeting.stream,
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (!snapshot.hasData || snapshot.data == null)
                return Container();

              if (snapshot.data.isEmpty) return Container();

              if (snapshot.hasData) {
                var meetList = snapshot.data;
                return CarouselSlider.builder(
                  options: CarouselOptions(
                    autoPlay: true,
                    aspectRatio: 2.0,
                    enlargeCenterPage: false,
                    viewportFraction: 1,
                    height: width * 0.65,
                    //            enableInfiniteScroll: false,
                  ),
                  itemCount: (meetList.length / 2).round(),
                  itemBuilder: (context, index) {
                    final int first = index * 2;
                    final int second = first + 1;
                    var imageUser = meetList[index]['game_thumbnail'] == null
                        ? domain + "assets/img/nopicture.jpg"
                        : domain +
                            "assets/img/" +
                            meetList[index]['game_thumbnail'];
                    return Row(
                      children: [first, second].map((idx) {
                        return Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.all(width * 0.02),
                            child: Card(
                              child: Column(
                                children: [
                                  Image.network(imageUser, fit: BoxFit.cover),
                                  Container(
                                    alignment: Alignment.topLeft,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width * 0.01),
                                    child: Text(
                                      meetList[idx]['meeting_name'],
                                      maxLines: 1,
                                      style: TextStyle(fontSize: width * 0.03),
                                    ),
                                  ),
                                  Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: width * 0.01),
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.people,
                                            color: Colors.grey,
                                          ),
                                          SizedBox(
                                            width: width * 0.02,
                                          ),
                                          Text(meetList[idx]['meeting_member'],
                                              style: TextStyle(
                                                  fontSize: width * 0.03,
                                                  color: Colors.grey))
                                        ],
                                      ))
                                ],
                              ),
                            ),
                          ),
                        );
                      }).toList(),
                    );
                  },
                );
              } else {
                return buildLoadingWidget();
              }
            }),
      ],
    );
  }

  Widget contentNews() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("News"),
          trailing: GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) {
                return ResponsiveWidget(builder: (context, constraints) {
                  return NewsScreen();
                });
              }));
            },
            child: Text(
              "See more...",
              style: TextStyle(color: Color(0xff4684DB)),
            ),
          ),
        ),
        Container(
          child: StreamBuilder<dynamic>(
              stream: homeBloc.homeNews.stream,
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  var data = snapshot.data;
                  return ListView.builder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      itemCount: data.length,
                      itemBuilder: (context, index) {
                        Color color = Color(0xffF5F2F2);
                        if (index % 2 == 0) {
                          color = Color(0xffFFFFFF);
                        }
                        return ColoredBox(
                          color: color,
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: width * 0.03,
                                vertical: width * 0.01),
                            child: Row(
                              children: [
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(10.0),
                                    child: Image.network(
                                      data[index]['attach'],
                                      width: width * 0.25,
                                      height: width * 0.25,
                                      fit: BoxFit.fill,
                                    )),
                                SizedBox(
                                  width: 10,
                                ),
                                Flexible(
                                    child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width * 0.01),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        data[index]["content1"],
                                        maxLines: 2,
                                      ),
                                      Text(
                                        "1 jam lalu",
                                        style: TextStyle(color: Colors.grey),
                                      )
                                    ],
                                  ),
                                )),
                              ],
                            ),
                          ),
                        );
                      });
                } else {
                  return buildLoadingWidget();
                }
              }),
        ),
      ],
    );
  }
}
