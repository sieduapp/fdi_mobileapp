import 'package:fdi/blocs/gmae_bloc.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:fdi/screens/game_play_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/html_parser.dart';
import 'package:progress_dialog/progress_dialog.dart';

class GameDetailScreen extends StatefulWidget {
  GameDetailScreen({Key key, @required this.gameId, this.gameName})
      : super(key: key);
  final String gameId;
  final String gameName;

  @override
  _GameDetailScreenState createState() => _GameDetailScreenState();
}

class _GameDetailScreenState extends State<GameDetailScreen> {
  ProgressDialog progressDialog;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _onLoading();
    _getData();
  }

  void _getData() {
    gameBloc..getGameDetail(widget.gameId);
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            widget.gameName,
            style:
                TextStyle(color: Color.fromRGBO(72, 72, 72, 1.0), fontSize: 40),
          ),
          backgroundColor: Colors.white,
        ),
        body: SingleChildScrollView(
          child: Container(
            child: StreamBuilder<dynamic>(
                stream: gameBloc.gameStream.stream,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return buildLoadingWidget();
                  var data = snapshot.data;
                  return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Stack(
                          children: [
                            Container(
                              width: width,
                              height: width * 0.5,
                              margin: EdgeInsets.symmetric(
                                  horizontal: width * 0.01),
                              child: data != null
                                  ? Image.network(data['game_thumbnail'],
                                      fit: BoxFit.fill)
                                  : Container(),
                            ),
                            Positioned(
                              bottom: width * 0.05,
                              left: width * 0.05,
                              child: Container(
                                child: Row(
                                  children: [
                                    Container(
                                      decoration:
                                          BoxDecoration(color: Colors.grey),
                                      child: Text(
                                        data != null
                                            ? data['game_category_name']
                                            : "",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: width * 0.02,
                                          vertical: width * 0.003),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        // Container(
                        //   margin: EdgeInsets.symmetric(
                        //       horizontal: width * 0.03, vertical: width * 0.03),
                        //   child: Text(
                        //     "Aturan main",
                        //     style: TextStyle(
                        //         color: Color.fromRGBO(72, 72, 72, 1.0),
                        //         fontSize: 20,
                        //         fontWeight: FontWeight.bold),
                        //   ),
                        // ),
                        Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: width * 0.03, vertical: width * 0.03),
                          child: Html(
                            data: data != null ? data['game_howto'] : "",
                            customRender: {
                              "flutter": (RenderContext context, Widget child,
                                  attributes, _) {
                                return FlutterLogo(
                                  style: (attributes['horizontal'] != null)
                                      ? FlutterLogoStyle.horizontal
                                      : FlutterLogoStyle.markOnly,
                                  textColor: context.style.color,
                                  size: context.style.fontSize.size * 5,
                                );
                              },
                            },
                            onLinkTap: (url) {},
                            onImageTap: (src) {
                              print(src);
                            },
                            onImageError: (exception, stackTrace) {
                              print(exception);
                            },
                          ),
                        ),
                        Center(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (_) {
                                return GamePlayScreen(
                                  gameId: data['game_id'],
                                );
                              }));
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: width * 0.03,
                                  vertical: width * 0.03),
                              padding: EdgeInsets.symmetric(
                                  horizontal: width * 0.07,
                                  vertical: width * 0.02),
                              decoration: BoxDecoration(
                                  color: Colors.orange,
                                  borderRadius:
                                      BorderRadius.circular(width * 0.03)),
                              child: Text(
                                "PLAY NOW",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: width * 0.05),
                              ),
                            ),
                          ),
                        )
                      ]);
                }),
          ),
        ));
  }

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            height: MediaQuery.of(context).size.height * 0.1,
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new CircularProgressIndicator(
                    backgroundColor: Colors.white,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                new Text("Loading..."),
              ],
            ),
          ),
        );
      },
    );
    // new Future.delayed(new Duration(seconds: 3), () {
    //   Navigator.pop(context); //pop dialog
    //   // _login();
    // });
  }
}
