import 'package:fdi/blocs/home_bloc.dart';
import 'package:fdi/components/cons.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/html_parser.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_html/flutter_html.dart';

class TermConditionScreen extends StatefulWidget {
  TermConditionScreen({Key key}) : super(key: key);

  @override
  _TermConditionScreenState createState() => _TermConditionScreenState();
}

class _TermConditionScreenState extends State<TermConditionScreen> {
  @override
  void initState() {
    // TODO: implement initState
    homeBloc..getTerm();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            "Term & Conditions",
            style: TextStyle(
                color: Color.fromRGBO(247, 160, 97, 1.0),
                fontWeight: FontWeight.bold,
                fontSize: 30),
          ),
          backgroundColor: const Color(0xffeeeeee),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                  top: 20,
                  left: 20,
                ),
                child: StreamBuilder(
                  stream: homeBloc.homeTerm.stream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      var data = snapshot.data;
                      return Padding(
                        padding: const EdgeInsets.only(
                          top: 5,
                          left: 20,
                          right: 20,
                        ),
                        child: Html(
                          data: data,
                          customRender: {
                            "flutter": (RenderContext context, Widget child,
                                attributes, _) {
                              return FlutterLogo(
                                style: (attributes['horizontal'] != null)
                                    ? FlutterLogoStyle.horizontal
                                    : FlutterLogoStyle.markOnly,
                                textColor: context.style.color,
                                size: context.style.fontSize.size * 5,
                              );
                            },
                          },
                          onLinkTap: (url) {},
                          onImageTap: (src) {
                            print(src);
                          },
                          onImageError: (exception, stackTrace) {
                            print(exception);
                          },
                        ),
                      );
                    }
                    return buildLoadingWidget();
                  },
                ),
              ),
            ],
          ),
        ));
  }
}
