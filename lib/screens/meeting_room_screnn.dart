import 'package:fdi/screens/meeting_room_chat_screen.dart';
import 'package:flutter/material.dart';

import 'shared/responsive_widget.dart';

class MeetingRoomScreen extends StatefulWidget {
  MeetingRoomScreen({Key key}) : super(key: key);

  @override
  _MeetingRoomScreenState createState() => _MeetingRoomScreenState();
}

class _MeetingRoomScreenState extends State<MeetingRoomScreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
//      appBar: AppBar(
//        iconTheme: IconThemeData(
//            color: Colors.black
//        ),
//        title: Text("Power Pop Bubbles",style: TextStyle(color: Colors.black),),
//        backgroundColor: Colors.white,
//      ),
        body: SingleChildScrollView(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Stack(
          children: [
            Container(
              width: width,
              height: width * 0.7,
              margin: EdgeInsets.symmetric(horizontal: width * 0.01),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('lib/images/game_hot.png'),
                  fit: BoxFit.cover,
                  alignment: Alignment.topCenter,
                ),
//                          borderRadius: BorderRadius.circular(width * 0.04)
              ),
            ),
            Positioned(
              bottom: width * 0.05,
              left: width * 0.05,
              child: Container(
                child: Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(color: Colors.grey),
                      child: Text(
                        "Category 1",
                        style: TextStyle(color: Colors.white),
                      ),
                      padding: EdgeInsets.symmetric(
                          horizontal: width * 0.02, vertical: width * 0.003),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
        Container(
          padding: EdgeInsets.all(width * 0.03),
          color: Color.fromRGBO(245, 242, 242, 1.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Icon(Icons.people),
                  Text("(20)"),
                ],
              ),
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_) {
                        return ResponsiveWidget(
                          builder: (context, constraints) {
                            return MeetingRoomChatScreen();
                          },
                        );
                      }));
                    },
                    child: Icon(Icons.chat),
                  ),
                  Text("(0)"),
                ],
              ),
            ],
          ),
        ),
        Container(
          color: Colors.white,
          child: ListView.builder(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: 3,
              itemBuilder: (context, index) {
                return Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: width * 0.03, vertical: height * 0.01),
//                              margin: EdgeInsets.only(bottom: width * 0.03),
                      child: Row(
                        children: [
                          Container(
                            width: width * 0.3,
                            height: width * 0.3,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(width * 0.03),
                              child: Image.asset(
                                'lib/images/image_video.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Container(
                            width: width * 0.45,
                            margin:
                                EdgeInsets.symmetric(horizontal: width * 0.03),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Aldy Arviansyah $index",
                                  maxLines: 2,
                                  style: TextStyle(fontSize: width * 0.04),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: width * 0.03,
                      left: width * 0.05,
                      child: Row(
                        children: [
                          Icon(
                            Icons.adjust,
                            color: Colors.red,
                            size: width * 0.03,
                          ),
                          Text(
                            " Live",
                            style: TextStyle(
                                color: Colors.white, fontSize: width * 0.03),
                          )
                        ],
                      ),
                    )
                  ],
                );
              }),
        ),
      ]),
    ));
  }
}
