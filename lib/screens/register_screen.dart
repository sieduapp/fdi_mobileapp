import 'package:fdi/components/form.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Color(0xff5EAF1FB),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: height * 2,
              ),
              Positioned(
                top: width * 0.1,
                right: 0.0,
                left: 0.0,
                child: Container(
                  margin: EdgeInsets.all(width * 0.05),
                  padding: EdgeInsets.all(width * 0.05),
                  decoration: BoxDecoration(color: Colors.white),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          "Buat Akun Baru",
                          style: TextStyle(
                              color: Colors.blue,
                              fontSize: 30,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      MyTextFormField(
                        label: "Nama Lengkap",
                        hintText: "Masukan email Anda",
                      ),
                      MyTextFormField(
                        label: "Username",
                        hintText: "Masukan username Anda",
                      ),
                      MyTextFormField(
                        label: "Email",
                        hintText: "Masukan email Anda",
                      ),
                      MyTextFormField(
                        label: "No. Hanphone",
                        hintText: "Masukan no hanphone Anda",
                      ),
                      MyTextFormField(
                        label: "Password",
                        hintText: "Masukan password Anda",
                        isPassword: true,
                      ),
                      MyTextFormField(
                        label: "Password Konfirmasi",
                        hintText: "Masukan password konfirmasi Anda",
                        isPassword: true,
                      ),
                      Text(
                        "Age",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      CheckboxListTile(
                        title: Text('Dibawah 17 tahun'),
                        value: true,
                        onChanged: (bool value) {
                          setState(() {
                            timeDilation = value ? 10.0 : 1.0;
                          });
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                        contentPadding: EdgeInsets.all(0.0),
                      ),
                      CheckboxListTile(
                        title: Text('Diatas 17 tahun'),
                        value: true,
                        onChanged: (bool value) {
                          setState(() {
                            print(value);
                          });
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                        contentPadding: EdgeInsets.all(0.0),
                      ),
                      SizedBox(
                        height: width * 0.05,
                      ),
                      Text(
                        "Pilih Opsi Referral ID",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: width * 0.02,
                      ),
                      DropdownButtonFormField(
                        hint: Text("Select Your Friends"),
                        value: "Non Referral",
                        items: ["Non Referral", "Referral"].map((value) {
                          return DropdownMenuItem(
                            child: Text(value),
                            value: value,
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
//                            _valFriends = value;
                          });
                        },
                        isExpanded: true,
                        decoration: const InputDecoration(
                            border: const OutlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 0.0, horizontal: 15.0)),
                      ),
                      SizedBox(
                        height: width * 0.05,
                      ),
                      Text(
                        "Input Referral ID",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: width * 0.02,
                      ),
                      DropdownButtonFormField(
                        hint: Text("Input Referral ID"),
                        value: "Ahmad",
                        items: ["Ahmad", "Agus"].map((value) {
                          return DropdownMenuItem(
                            child: Text(value),
                            value: value,
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
//                            _valFriends = value;
                          });
                        },
                        isExpanded: true,
                        decoration: const InputDecoration(
                            border: const OutlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 0.0, horizontal: 15.0)),
                      ),
                      SizedBox(
                        height: width * 0.05,
                      ),
                      Container(
                          margin: EdgeInsets.symmetric(vertical: width * 0.01),
                          child:
//                          Checkbox(
//
//                          )
                              CheckboxListTile(
                            title: Row(
                              children: [
                                Text(
                                  'Setuju dengan ',
                                  style: TextStyle(fontSize: width * 0.03),
                                ),
                                Text(
                                  'Syarat & Ketentuan King Poin',
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontSize: width * 0.03),
                                ),
                              ],
                            ),
                            value: timeDilation != 1.0,
                            onChanged: (bool value) {
                              setState(() {
                                timeDilation = value ? 10.0 : 1.0;
                              });
                            },
                            controlAffinity: ListTileControlAffinity.leading,
                            contentPadding: EdgeInsets.all(0.0),
                          )),
                      MyButtonFormField(
                        label: "SIGN UP",
                        onSaved: () {},
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Belum memiliki akun? "),
                          GestureDetector(
                            onTap: () {},
                            child: Text(
                              "Daftar sekarang",
                              style: TextStyle(color: Colors.blue),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
