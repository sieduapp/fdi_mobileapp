import 'package:fdi/blocs/video_bloc.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:flutter/material.dart';
// import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import 'video_detail_screen.dart';

class VideoScreen extends StatefulWidget {
  VideoScreen({Key key}) : super(key: key);

  @override
  _VideoScreenState createState() => _VideoScreenState();
}

class _VideoScreenState extends State<VideoScreen> {
  final TextEditingController _search = TextEditingController();

  @override
  void initState() {
    videoBloc..getIdentityVideo();
    videoBloc..getVideosFeed();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: const Color(0xff284356),
          ),
          backgroundColor: const Color(0xffeeeeee),
          elevation: 0.0,
          title: Text(
            "Video",
            style: TextStyle(
                color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 30),
          ),
          actions: <Widget>[
            Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container(
                  height: height * 40,
                  width: width * 0.45,
                  child: TextFormField(
                    controller: _search,
                    decoration: InputDecoration(
                      hintText: "Search something..",
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.red,
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      contentPadding: const EdgeInsets.only(
                        top: 0.00,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    cursorColor: Colors.black,
                    style: TextStyle(color: Colors.black),
                  ),
                )),
          ],
        ),
        body: ListView(children: <Widget>[
          Column(children: [
            SingleChildScrollView(
                child: Column(
              children: [
                Container(
                  height: width * 0.8,
                  child: Stack(
                    children: [
                      Positioned(
                        top: width * 0.45,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: width * 0.3,
                          width: width * 1,
                          decoration: BoxDecoration(color: Color(0xffEAF1FB)),
                        ),
                      ),
                      Positioned(
                        top: width * 0.5,
                        right: width * -0.03,
                        child: Image(
                            image: AssetImage('lib/images/man_blue.png'),
                            width: width * 0.2,
                            fit: BoxFit.fill),
                      ),
                      Positioned(
                        top: width * 0.5,
                        left: width * -0.03,
                        child: Image(
                            image: AssetImage('lib/images/man_red.png'),
                            width: width * 0.2,
                            fit: BoxFit.fill),
                      ),
                      Positioned(
                        top: width * 0.05,
                        left: width * 0.05,
                        right: width * 0.05,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20.0),
                          child: contentVideo(),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Text(
                    "Videos Feed",
                    style: TextStyle(fontSize: width * 0.05),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(child: videosFeed()),
//                 Container(
// //                          width: width * 0.9,
//                   padding: EdgeInsets.all(width * 0.02),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       Text(
//                         "1 2 3 4 5 ... next",
//                         style: TextStyle(color: Colors.grey),
//                       ),
//                     ],
//                   ),
//                 )
              ],
            )),
          ]),
        ]));
  }

  Widget contentVideo() {
    return StreamBuilder<String>(
        stream: videoBloc.identityVideo.stream,
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            YoutubePlayerController _controller = YoutubePlayerController(
              initialVideoId: data,
              params: YoutubePlayerParams(
                // Defining custom playlist
                startAt: Duration(seconds: 30),
                showControls: true,
                showFullscreenButton: true,
              ),
            );
            return YoutubePlayerIFrame(
              controller: _controller,
              aspectRatio: 16 / 9,
            );
          } else {
            return buildLoadingWidget();
          }
        });
  }

  Widget videosFeed() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder<dynamic>(
        stream: videoBloc.videosFeed.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            return ListView.builder(
                shrinkWrap: true, // 1st add
                physics: ClampingScrollPhysics(),
                itemCount: data.length,
                itemBuilder: (context, index) {
                  YoutubePlayerController _controller = YoutubePlayerController(
                    initialVideoId: data[index]["video_youtubeid"],
                    params: YoutubePlayerParams(
                      // Defining custom playlist
                      startAt: Duration(seconds: 30),
                      showControls: true,
                      showFullscreenButton: true,
                    ),
                  );
                  return Container(
                      margin: EdgeInsets.all(width * 0.04),
                      decoration: BoxDecoration(
                          color: Color(0xff4684DB),
                          borderRadius: BorderRadius.circular(width * 0.05)),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: YoutubePlayerIFrame(
                              controller: _controller,
                              aspectRatio: 16 / 9,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: width * 0.02,
                                horizontal: width * 0.05),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: width * 0.50,
                                      child: Text(
                                        data[index]["video_title"],
                                        maxLines: 1,
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: width * 0.007,
                                          horizontal: width * 0.01),
                                      decoration: BoxDecoration(
                                          color: Color(0xff5492E9),
                                          borderRadius:
                                              BorderRadius.circular(5.0)),
                                      child: Text(
                                        data[index]["video_category_name"],
                                        style: TextStyle(
                                            color: Color(0xff3D6EB2),
                                            fontSize: width * 0.03),
                                      ),
                                    )
                                  ],
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (_) {
                                      return VideoDetailScreen(
                                        youtube_id: data[index]
                                            ["video_youtubeid"],
                                      );
                                    }));
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width * 0.04,
                                        vertical: width * 0.02),
                                    decoration: BoxDecoration(
                                      color: Color(0xff3A6DB5),
                                      borderRadius:
                                          BorderRadius.circular(width * 0.02),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xff3A6DB5),
                                          spreadRadius: 1,
                                          blurRadius: 1,
//                                              offset: Offset(0, 3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Text(
                                      "Watch Now",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ));
                });
          } else {
            return buildLoadingWidget();
          }
        });
  }
}
