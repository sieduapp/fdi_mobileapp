import 'package:fdi/blocs/video_bloc.dart';
import 'package:fdi/elements/iklan_element.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class VideoDetailScreen extends StatefulWidget {
  VideoDetailScreen({Key key, @required this.youtube_id}) : super(key: key);
  String youtube_id;

  @override
  _VideoDetailScreenState createState() => _VideoDetailScreenState();
}

class _VideoDetailScreenState extends State<VideoDetailScreen> {
  final TextEditingController _search = TextEditingController();

  String title = "";
  String descripsi = "";

  @override
  void initState() {
    videoBloc..getVideoDetail("${widget.youtube_id}");
    videoBloc..getVideosFeed();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return WillPopScope(
      child: Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: const Color(0xff284356),
            ),
            backgroundColor: const Color(0xffeeeeee),
            elevation: 0.0,
            title: Text(
              "Video",
              style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 30),
            ),
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Container(
                    height: height * 40,
                    width: width * 0.45,
                    child: TextFormField(
                      controller: _search,
                      decoration: InputDecoration(
                        hintText: "Search something..",
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.red,
                        ),
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: const EdgeInsets.only(
                          top: 0.00,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: const Color(0x6bffffff)),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: const Color(0x6bffffff)),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                      cursorColor: Colors.black,
                      style: TextStyle(color: Colors.black),
                    ),
                  )),
            ],
          ),
          body: SingleChildScrollView(
            child: Column(children: [
              buildIklan(null),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [contentVideo()],
              ),
              SizedBox(
                height: width * 0.05,
              ),
              Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.symmetric(
                    horizontal: width * 0.05, vertical: width * 0.01),
                child: Text(
                  "Video Lainnya",
                  style: TextStyle(fontSize: width * 0.05),
                ),
              ),
              Container(
                height: width * 0.45,
                child: videoLainnya(),
              )
            ]),
          )),
      onWillPop: () {
        Navigator.pop(context);
      },
    );
  }

  Widget contentVideo() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder<dynamic>(
        stream: videoBloc.videoDetail.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            YoutubePlayerController _controller = YoutubePlayerController(
              initialVideoId: data["video_youtubeid"],
              params: YoutubePlayerParams(
                // Defining custom playlist
                startAt: Duration(seconds: 0),
                showControls: true,
                showFullscreenButton: true,
              ),
            );
            title = data["video_title"];
            descripsi = data["video_description"];
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: width * 0.8,
                  child: Stack(
                    children: [
                      Positioned(
                        top: width * 0.45,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: width * 0.3,
                          width: width * 1,
                          decoration: BoxDecoration(color: Color(0xffEAF1FB)),
                        ),
                      ),
                      Positioned(
                        top: width * 0.5,
                        right: width * -0.03,
                        child: Image(
                            image: AssetImage('lib/images/man_blue.png'),
                            width: width * 0.2,
                            fit: BoxFit.fill),
                      ),
                      Positioned(
                        top: width * 0.5,
                        left: width * -0.03,
                        child: Image(
                            image: AssetImage('lib/images/man_red.png'),
                            width: width * 0.2,
                            fit: BoxFit.fill),
                      ),
                      Positioned(
                        top: width * 0.05,
                        left: width * 0.05,
                        right: width * 0.05,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20.0),
                          child: YoutubePlayerIFrame(
                            controller: _controller,
                            aspectRatio: 16 / 9,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: width * 0.05),
                  child: Text(
                    title ?? "-",
                    style: TextStyle(fontSize: width * 0.08),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: width * 0.05, vertical: width * 0.01),
                  child: Text(
                    "Deskripsi",
                    style: TextStyle(fontSize: width * 0.05),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: width * 0.05, vertical: width * 0.01),
                  child: Text(
                    descripsi ?? "-",
                    style:
                        TextStyle(fontSize: width * 0.04, color: Colors.grey),
                  ),
                ),
              ],
            );
          } else {
            return buildLoadingWidget();
          }
        });
  }

  Widget videoLainnya() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder<dynamic>(
        stream: videoBloc.videosFeed.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            return ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                physics: ClampingScrollPhysics(),
                itemCount: data.length,
                itemBuilder: (context, index) {
                  YoutubePlayerController _controller = YoutubePlayerController(
                    initialVideoId: data[index]["video_youtubeid"],
                    params: YoutubePlayerParams(
                      // Defining custom playlist
                      startAt: Duration(seconds: 30),
                      showControls: true,
                      showFullscreenButton: true,
                    ),
                  );
                  return Container(
                      width: width * 0.4,
                      margin: EdgeInsets.all(width * 0.04),
                      decoration: BoxDecoration(
                          color: Color(0xff4684DB),
                          borderRadius: BorderRadius.circular(width * 0.03)),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(width * 0.03),
                            child: YoutubePlayerIFrame(
                              controller: _controller,
                              aspectRatio: 16 / 9,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: width * 0.02,
                                horizontal: width * 0.05),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (_) {
                                  return VideoDetailScreen(
                                      youtube_id: data[index]
                                          ["video_youtubeid"]);
                                }));
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * 0.04,
                                    vertical: width * 0.02),
                                decoration: BoxDecoration(
                                  color: Color(0xff3A6DB5),
                                  borderRadius:
                                      BorderRadius.circular(width * 0.02),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xff3A6DB5),
                                      spreadRadius: 1,
                                      blurRadius: 1,
//                                              offset: Offset(0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: Text(
                                  "Watch Now",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ));
                });
          } else {
            return buildLoadingWidget();
          }
        });
  }
}
