import 'package:flutter/material.dart';

class ForgotPasswordScreen extends StatefulWidget {
  ForgotPasswordScreen({Key key}) : super(key: key);

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Color(0xff5EAF1FB),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: height,
              ),
              Positioned(
                top: width * 0.3,
                right: 0.0,
                left: 0.0,
                child: Container(
                  margin: EdgeInsets.all(width * 0.05),
                  padding: EdgeInsets.all(width * 0.05),
                  decoration: BoxDecoration(color: Colors.white),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Lupa Password",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: width * 0.01),
                        child: TextFormField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Color(0xff03A9F4))),
                            contentPadding: EdgeInsets.all(10.0),
                            hintText: "Masukan email Anda",
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(width * 0.02),
                        child: Text("Password akan dikirimkan ke email Anda"),
                      ),
                      SizedBox(
                        height: width * 0.15,
                      ),
                      Container(
                          child: ButtonTheme(
                        minWidth: width,
                        height: width * 0.11,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(color: Colors.transparent)),
                          onPressed: () {},
                          color: Colors.blue,
                          textColor: Colors.white,
                          child: Text("KIRIM"),
                        ),
                      )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
