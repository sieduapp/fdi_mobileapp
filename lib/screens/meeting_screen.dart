import 'dart:io';

import 'package:fdi/blocs/home_bloc.dart';
import 'package:fdi/components/cons.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:fdi/screens/meeting_form_screen.dart';
import 'package:fdi/screens/meeting_room_screnn.dart';
import 'package:flutter/material.dart';
import 'package:jitsi_meet/feature_flag/feature_flag_enum.dart';
import 'package:jitsi_meet/jitsi_meet.dart';
import 'package:jitsi_meet/jitsi_meeting_listener.dart';
import 'package:jitsi_meet/room_name_constraint.dart';
import 'package:jitsi_meet/room_name_constraint_type.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'shared/responsive_widget.dart';

class MeetingScreen extends StatefulWidget {
  MeetingScreen({Key key}) : super(key: key);

  @override
  _MeetingScreenState createState() => _MeetingScreenState();
}

class _MeetingScreenState extends State<MeetingScreen> {
  final TextEditingController _search = TextEditingController();
  var isAudioOnly = true;
  var isAudioMuted = true;
  var isVideoMuted = true;

  var search = "";

  @override
  void initState() {
    super.initState();
    homeBloc..getHomeMeeting();

    JitsiMeet.addListener(JitsiMeetingListener(
        onConferenceWillJoin: _onConferenceWillJoin,
        onConferenceJoined: _onConferenceJoined,
        onConferenceTerminated: _onConferenceTerminated,
        onError: _onError));
    setState(() {
      search = search;
    });
  }

  @override
  void dispose() {
    super.dispose();
    JitsiMeet.removeAllListeners();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: const Color(0xff284356),
          ),
          backgroundColor: const Color(0xffeeeeee),
          elevation: 0.0,
          title: Text(
            "Meeting Room",
            style: TextStyle(
                color: Colors.orange,
                fontWeight: FontWeight.bold,
                fontSize: 20),
          ),
          actions: <Widget>[
            Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container(
                  height: height * 40,
                  width: width * 0.45,
                  child: TextFormField(
                    controller: _search,
                    onChanged: (text){
                      setState(() {search=text;});
                    },
                    decoration: InputDecoration(
                      hintText: "Search something..",
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.red,
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      contentPadding: const EdgeInsets.only(
                        top: 0.00,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: const Color(0x6bffffff)),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    cursorColor: Colors.black,
                    style: TextStyle(color: Colors.black),
                  ),
                )),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            var IsAuthenticated = await prefs.getBool("IsAuthenticated");
            if(IsAuthenticated!=null){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MeetingForm()),
              );
            } else {
              _showMyDialogLogin();
            }
          },
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          backgroundColor: const Color(0xff284356),
        ),
        body: ListView(children: [
          Column(children: [
            _contentHeader(),
            Container(
              margin: EdgeInsets.all(width * 0.05),
              child: Text(
                "Meeting Room Feed",
                style: TextStyle(fontSize: width * 0.09),
              ),
            ),
            _contentList(),
          ]),
        ]));
  }

  Widget _contentHeader() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder<dynamic>(
        stream: homeBloc.homeMeeting.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            var imageUser = data[0]['img'] == null
                ? domain + "assets/img/nopicture.jpg"
                : domain + "assets/img/" + data[0]['img'];
            return Stack(
              children: [
                Container(
                  width: width,
                  height: width * 0.6,
                  child: Image.network(
                    imageUser,
                    width: width * 1.0,
                    height: height * 0.4,
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                  top: width * 0.05,
                  left: width * 0.05,
                  child: Container(
                    child: Row(
                      children: [
                        Icon(
                          Icons.adjust,
                          color: Colors.red,
                          size: width * 0.05,
                        ),
                        Text(
                          " Live",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  bottom: width * 0.05,
                  left: width * 0.05,
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          data[0]['name'],
                          style: TextStyle(
                              color: Colors.white, fontSize: width * 0.04),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.people,
                              color: Colors.white,
                              size: width * 0.04,
                            ),
                            Text(
                              data[0]['meeting_member'],
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: width * 0.03,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            );
          } else {
            return buildLoadingWidget();
          }
        });
  }

  Widget _contentList() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder<dynamic>(
        stream: homeBloc.homeMeeting.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          var data = [];
          if (snapshot.hasData) {
            var meeting = snapshot.data;

            if(search!=''){
              meeting.forEach((temp) {
                if ((temp['name'].toLowerCase()).contains(search.toLowerCase()))
                  data.add(temp);
              });
            } else {
              data = meeting;
            }

            return Container(
                color: Colors.white,
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      var imageUser = data[index]['img'] == null
                          ? domain + "assets/img/nopicture.jpg"
                          : domain + "assets/img/" + data[index]['img'];
                      return Stack(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: width * 0.03,
                                vertical: width * 0.01),
                            margin: EdgeInsets.only(bottom: width * 0.03),
                            child: Row(
                              children: [
                                Container(
                                  width: width * 0.3,
                                  height: width * 0.3,
                                  child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.circular(width * 0.03),
                                    child: Image.network(
                                      imageUser,
                                      width: width * 1.0,
                                      height: height * 0.4,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: width * 0.45,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: width * 0.03),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        data[index]['name'],
                                        maxLines: 2,
                                        style:
                                            TextStyle(fontSize: width * 0.04),
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                            vertical: width * 0.02),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.people,
                                              color: Colors.grey,
                                              size: width * 0.04,
                                            ),
                                            Text(
                                              data[index]['meeting_member'],
                                              style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: width * 0.03,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          _joinMeeting(
                                              data[index]['meeting_name'] +
                                                  data[index]['meeting_id'],
                                              data[index]['name'],
                                              data[index]['email']);
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      width * 0.03),
                                              color: Colors.green),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: width * 0.03,
                                              vertical: width * 0.01),
//                                                color: Colors.green,
                                          child: Text(
                                            "JOIN NOW",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: width * 0.03,
                            left: width * 0.05,
                            child: Row(
                              children: [
                                Icon(
                                  Icons.adjust,
                                  color: Colors.red,
                                  size: width * 0.03,
                                ),
                                Text(
                                  " Live",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: width * 0.03),
                                )
                              ],
                            ),
                          )
                        ],
                      );
                    }));
          } else {
            return buildLoadingWidget();
          }
        });
  }

  _onAudioOnlyChanged(bool value) {
    setState(() {
      isAudioOnly = value;
    });
  }

  _onAudioMutedChanged(bool value) {
    setState(() {
      isAudioMuted = value;
    });
  }

  _onVideoMutedChanged(bool value) {
    setState(() {
      isVideoMuted = value;
    });
  }

  _joinMeeting(String room, String name, String email) async {
    String serverUrl = "https://kingpoin.1980media.com/";

    try {
      // Enable or disable any feature flag here
      // If feature flag are not provided, default values will be used
      // Full list of feature flags (and defaults) available in the README
      Map<FeatureFlagEnum, bool> featureFlags = {
        FeatureFlagEnum.WELCOME_PAGE_ENABLED: false,
      };

      // featureFlags[FeatureFlagEnum.MEETING_PASSWORD_ENABLED] = true;
      // Here is an example, disabling features for each platform
      if (Platform.isAndroid) {
        // Disable ConnectionService usage on Android to avoid issues (see README)
        featureFlags[FeatureFlagEnum.CALL_INTEGRATION_ENABLED] = false;
      } else if (Platform.isIOS) {
        // Disable PIP on iOS as it looks weird
        featureFlags[FeatureFlagEnum.PIP_ENABLED] = false;
      }

      // Define meetings options here
      room = room.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
      var options = JitsiMeetingOptions()
        ..room = room
        ..serverURL = serverUrl
        ..subject = room
        ..userDisplayName = "Aldy Arviansyah" //name
        ..userEmail = "aldyarviansyah@gmail.com" //email
        ..audioOnly = isAudioOnly
        ..audioMuted = isAudioMuted
        ..videoMuted = isVideoMuted
        ..featureFlags.addAll(featureFlags);

      debugPrint("JitsiMeetingOptions: $options");
      await JitsiMeet.joinMeeting(
        options,
        listener: JitsiMeetingListener(onConferenceWillJoin: ({message}) {
          debugPrint("${options.room} will join with message: $message");
        }, onConferenceJoined: ({message}) {
          debugPrint("${options.room} joined with message: $message");
        }, onConferenceTerminated: ({message}) {
          debugPrint("${options.room} terminated with message: $message");
        }),
        // by default, plugin default constraints are used
        //roomNameConstraints: new Map(), // to disable all constraints
        //roomNameConstraints: customContraints, // to use your own constraint(s)
      );
    } catch (error) {
      debugPrint("error: $error");
    }
  }

  static final Map<RoomNameConstraintType, RoomNameConstraint>
      customContraints = {
    RoomNameConstraintType.MAX_LENGTH: new RoomNameConstraint((value) {
      return value.trim().length <= 50;
    }, "Maximum room name length should be 30."),
    RoomNameConstraintType.FORBIDDEN_CHARS: new RoomNameConstraint((value) {
      return RegExp(r"[$€£]+", caseSensitive: false, multiLine: false)
              .hasMatch(value) ==
          false;
    }, "Currencies characters aren't allowed in room names."),
  };

  void _onConferenceWillJoin({message}) {
    debugPrint("_onConferenceWillJoin broadcasted with message: $message");
  }

  void _onConferenceJoined({message}) {
    debugPrint("_onConferenceJoined broadcasted with message: $message");
  }

  void _onConferenceTerminated({message}) {
    debugPrint("_onConferenceTerminated broadcasted with message: $message");
  }

  _onError(error) {
    debugPrint("_onError broadcasted: $error");
  }

  Future<void> _showMyDialogLogin() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Peringatan'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Maaf, Anda harus login terlebih dahulu!'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
