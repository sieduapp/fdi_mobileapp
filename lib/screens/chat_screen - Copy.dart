import 'package:cached_network_image/cached_network_image.dart';
import 'package:fdi/blocs/chat_bloc.dart';
import 'package:fdi/components/cons.dart';
import 'package:fdi/controllers/firebaseController.dart';
import 'package:fdi/elements/loader_element.dart';
import 'package:fdi/models/chat_model.dart';
import 'package:fdi/screens/chat_king_law_screen.dart';
import 'package:fdi/screens/chat_room_screen.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'chat_king_health_screen.dart';
import 'shared/responsive_widget.dart';

class ChatScreen extends StatefulWidget {
  ChatScreen({Key key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  void initState() {
    super.initState();
    firebaseControl();
  }

  DatabaseReference _messagesRef;
  FirebaseDatabase dbRef;
  void firebaseControl() async {
    dbRef = await FirebaseController.instanace.config();
    chatBloc..getChatList();
    // _messagesRef = dbRef.reference().child("messages");
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
//      appBar: AppBar(
//        title: Text(widget.title),
//      ),
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xffF1F8F4),
      body: SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.all(width * 0.03),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Container(
                      width: width * 0.55,
                      child: DropdownButtonFormField(
                        // hint: Text("Input Referral ID"),
                        value: "Member Chat",
                        items: ["Member Chat"].map((value) {
                          return DropdownMenuItem(
                            child: Text(value),
                            value: value,
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
//                            _valFriends = value;
                          });
                        },
                        isExpanded: true,
                        decoration: const InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          border: const OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 0.0, horizontal: 15.0),
                        ),
                        style: TextStyle(
                            color: Color.fromRGBO(41, 130, 75, 1.0),
                            fontSize: 30,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: width * 0.03),
                      child: ButtonTheme(
                        minWidth: width * 0.1,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: RaisedButton(
                          child: Image(
                              image: AssetImage('lib/images/chat_filter.png'),
                              height: width * 0.05,
                              width: width * 0.05,
                              fit: BoxFit.fitWidth),
                          color: Colors.white,
                          onPressed: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (_) {
                              return ResponsiveWidget(
                                builder: (context, constraints) {
                                  return ChatKingHealthScreen();
                                },
                              );
                            }));
                          },
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: width * 0.03),
                      child: ButtonTheme(
                        minWidth: width * 0.1,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: RaisedButton(
                          child: Image(
                              image: AssetImage('lib/images/chat_add.png'),
                              height: width * 0.05,
                              width: width * 0.05,
                              fit: BoxFit.fitWidth),
                          color: Colors.white,
                          onPressed: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (_) {
                              return ResponsiveWidget(
                                builder: (context, constraints) {
                                  return ChatKingLawScreen();
                                },
                              );
                            }));
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: width * 0.02),
                  child: Row(
                    children: [
                      Container(
                        width: width * 0.75,
                        child: TextFormField(
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            border: InputBorder.none,
                            hintText: "Cari pesan ...",
                          ),
                        ),
                      ),
                      ButtonTheme(
                        minWidth: width * 0.1,
                        height: width * 0.10,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0)),
                        ),
                        child: RaisedButton(
                          color: Color(0xffE2E1E1),
                          child: Icon(
                            Icons.search,
                            color: Colors.grey,
                          ),
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
                ),
                _listChat(context),
              ],
            )),
      ),
    );
  }

  Widget _listChat(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      child: StreamBuilder<dynamic>(
        stream: chatBloc.chat.stream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;
            return ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return StreamBuilder(
                      stream: dbRef
                          .reference()
                          .child("messages")
                          .orderByChild("halaman")
                          .equalTo(data[index]["user_id"])
                          .limitToLast(1)
                          .onValue,
                      builder: (context, chatListSnapshot) {
                        var imageUser = data[index]['img'] == null ||
                                data[index]['img'] == ""
                            ? domain + "assets/img/nopicture.jpg"
                            : domain + "assets/img/" + data[index]['img'];
                        if (data[index]['is_group'] == 1) {
                          imageUser = domain + "assets/img/group.png";
                        }

                        Map dataMap = chatListSnapshot.data != null
                            ? chatListSnapshot.data.snapshot.value
                            : null;
                        return (chatListSnapshot.hasData)
                            ? ListTile(
                                leading: ClipRRect(
                                  borderRadius: BorderRadius.circular(30),
                                  child: CachedNetworkImage(
                                    imageUrl: imageUser,
                                    placeholder: (context, url) => Container(
                                      transform:
                                          Matrix4.translationValues(0, 0, 0),
                                      child: Container(
                                          width: 60,
                                          height: 80,
                                          child: Center(
                                              child:
                                                  new CircularProgressIndicator())),
                                    ),
                                    errorWidget: (context, url, error) =>
                                        new Icon(Icons.error),
                                    width: 60,
                                    height: 80,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                title: Text(data[index]['name']),
                                subtitle: Text((chatListSnapshot.hasData &&
                                        dataMap != null)
                                    ? dataMap.values.single["message"]
                                    : ""),
                                trailing: Container(
                                  width: 60,
                                  height: 50,
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        (chatListSnapshot.hasData)
                                            ? readTimestamp(DateTime.now()
                                                .millisecondsSinceEpoch)
                                            : readTimestamp(DateTime.now()
                                                .millisecondsSinceEpoch),
                                        style: TextStyle(fontSize: 12),
                                      ),
                                      InkWell(
                                        onTap: () async {
                                          final position =
                                              buttonMenuPosition(context);
                                          final result = await showMenu(
                                            context: context,
                                            position: position,
                                            items: <PopupMenuItem<String>>[
                                              PopupMenuItem<String>(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: <Widget>[
                                                      Icon(Icons
                                                          .delete),
                                                      Text("Delete",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black)),
                                                    ],
                                                  ),
                                                  value: "Delete",
                                                  textStyle: TextStyle(
                                                      color: Colors.black))
                                            ],
                                          );
                                        },
                                        child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 5, 0, 0),
                                            child: Icon(Icons.more_vert)),
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  _moveTochatRoom();
                                },
                                onLongPress: () async {
                                  final position = buttonMenuPosition(context);
                                  final result = await showMenu(
                                    context: context,
                                    position: position,
                                    items: <PopupMenuItem<String>>[
                                      PopupMenuItem<String>(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Icon(
                                                  Icons.delete),
                                              Text("Delete",
                                                  style: TextStyle(
                                                      color: Colors.black)),
                                            ],
                                          ),
                                          value: "Delete",
                                          textStyle:
                                              TextStyle(color: Colors.black))
                                    ],
                                  );
                                },
                              )
                            : ListTile(
                                leading: ClipRRect(
                                  borderRadius: BorderRadius.circular(15),
                                  child: CachedNetworkImage(
                                    imageUrl: imageUser,
                                    placeholder: (context, url) => Container(
                                      transform:
                                          Matrix4.translationValues(0, 0, 0),
                                      child: Container(
                                          width: 60,
                                          height: 80,
                                          child: Center(
                                              child:
                                                  new CircularProgressIndicator())),
                                    ),
                                    errorWidget: (context, url, error) =>
                                        new Icon(Icons.error),
                                    width: 60,
                                    height: 80,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                title: Text(data[index]['name']),
                                subtitle: Text(""),
                                trailing: Container(
                                  width: 60,
                                  height: 50,
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        (chatListSnapshot.hasData)
                                            ? readTimestamp(DateTime.now()
                                                .millisecondsSinceEpoch)
                                            : readTimestamp(DateTime.now()
                                                .millisecondsSinceEpoch),
                                        style: TextStyle(fontSize: 12),
                                      ),
                                      InkWell(
                                        onTap: () async {
                                          final position =
                                              buttonMenuPosition(context);
                                          final result = await showMenu(
                                            context: context,
                                            position: position,
                                            items: <PopupMenuItem<String>>[
                                              PopupMenuItem<String>(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: <Widget>[
                                                      Icon(Icons
                                                          .delete),
                                                      Text("Delete",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black)),
                                                    ],
                                                  ),
                                                  value: "Delete",
                                                  textStyle: TextStyle(
                                                      color: Colors.black))
                                            ],
                                          );
                                        },
                                        child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 5, 0, 0),
                                            child: Icon(Icons.more_vert)),
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  _moveTochatRoom();
                                },
                                onLongPress: () async {
                                  final position = buttonMenuPosition(context);
                                  final result = await showMenu(
                                    context: context,
                                    position: position,
                                    items: <PopupMenuItem<String>>[
                                      PopupMenuItem<String>(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Icon(
                                                  Icons.delete),
                                              Text("Delete",
                                                  style: TextStyle(
                                                      color: Colors.black)),
                                            ],
                                          ),
                                          value: "Delete",
                                          textStyle:
                                              TextStyle(color: Colors.black))
                                    ],
                                  );
                                });
                      });
                });
          }
          return buildLoadingWidget();
        },
      ),
    );
  }

  RelativeRect buttonMenuPosition(BuildContext c) {
    final RenderBox bar = c.findRenderObject();
    final RenderBox overlay = Overlay.of(c).context.findRenderObject();
    final RelativeRect position = RelativeRect.fromRect(
      Rect.fromPoints(
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
      ),
      Offset.zero & overlay.size,
    );
    return position;
  }

  Future<void> _moveTochatRoom() async {
    try {
      // Navigator.push(
      //     context,
      //     MaterialPageRoute(
      //         builder: (context) => ChatRoom(
      //             widget.myID,
      //             widget.myName,
      //             selectedUserID,
      //             selectedUserName,
      //             selectedUserThumbnail)));
    } catch (e) {
      print(e.message);
    }
  }

  String readTimestamp(int timestamp) {
    var now = DateTime.now();
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp);
    var diff = now.difference(date);
    var time = '';

    if (diff.inSeconds <= 0 ||
        diff.inSeconds > 0 && diff.inMinutes == 0 ||
        diff.inMinutes > 0 && diff.inHours == 0 ||
        diff.inHours > 0 && diff.inDays == 0) {
      if (diff.inHours > 0) {
        time = diff.inHours.toString() + 'h ago';
      } else if (diff.inMinutes > 0) {
        time = diff.inMinutes.toString() + 'm ago';
      } else if (diff.inSeconds > 0) {
        time = 'now';
      } else if (diff.inMilliseconds > 0) {
        time = 'now';
      } else if (diff.inMicroseconds > 0) {
        time = 'now';
      } else {
        time = 'now';
      }
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      time = diff.inDays.toString() + 'd ago';
    } else if (diff.inDays > 6) {
      time = (diff.inDays / 7).floor().toString() + 'w ago';
    } else if (diff.inDays > 29) {
      time = (diff.inDays / 30).floor().toString() + 'm ago';
    } else if (diff.inDays > 365) {
      time = '${date.month}-${date.day}-${date.year}';
    }
    return time;
  }
}
