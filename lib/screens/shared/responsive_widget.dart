import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'size_information.dart';

class ResponsiveWidget extends StatelessWidget {
  final AppBar appBar;
  final Drawer drawer;
  final Widget Function(BuildContext context, SizeInformation constraints)
      builder;
  ResponsiveWidget({@required this.builder, this.appBar, this.drawer});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var orientation = MediaQuery.of(context).orientation;

    SizeInformation information = SizeInformation(width, height, orientation);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: appBar,
        drawer: drawer,
        body: Builder(
          builder: (context) {
            return builder(context, information);
          },
        ),
      ),
    );
  }
}
