import 'package:dio/dio.dart';
import 'package:fdi/components/cons.dart';

class Repository {
  static String mainUrl = domain + "api/apimobile";

  //URL API
  static String _getIdentityVideo = "$mainUrl/getidentityvideo";
  static String _getHomeGames = "$mainUrl/gethomegames";
  static String _getHomeMeeting = "$mainUrl/gethomemeeting";
  static String _getHomeNews = "$mainUrl/getHomeNews";
  static String _getVideosFeed = "$mainUrl/getVideosFeed";
  static String _getVideoDetail = "$mainUrl/getVideoDetail?";
  static String _getGameDetail = "$mainUrl/getGameDetail?";
  static String _login = "$mainUrl/login?";
  static String _getUser = "$mainUrl/getuser?";
  static String _getReferral = "$mainUrl/getReferral?";
  static String _editUser = "$mainUrl/editUser";
  static String _getListChat = "$mainUrl/ListOfChat?";
  static String _lupaPassword = "$mainUrl/lupaPassword?";
  static String _getNewsDetail = "$mainUrl/getNewsDetail?";
  static String _getTerm = "$mainUrl/identityTerm";

  static String _addMeetingRoom = "$mainUrl/addmeeting";

  Future<String> getIdentityVideo() async {
    try {
      Response response = await Dio().get(_getIdentityVideo);
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getHomeGames() async {
    try {
      Response response = await Dio().get(_getHomeGames);
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getHomeMeeting() async {
    try {
      Response response = await Dio().get(_getHomeMeeting);
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getHomeNews() async {
    try {
      Response response = await Dio().get(_getHomeNews);
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getVideosFeed() async {
    try {
      Response response = await Dio().get(_getVideosFeed);
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getVideoDetail(String youtubeId) async {
    var params = {
      "youtube_id": youtubeId,
    };
    try {
      Response response = await Dio().get(
        _getVideoDetail,
        queryParameters: params,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getgameDetail(String gameId) async {
    var params = {
      "gameId": gameId,
    };
    try {
      Response response = await Dio().get(
        _getGameDetail,
        queryParameters: params,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> login(String email, String password) async {
    var params = {
      "email": email,
      "password": password,
    };
    try {
      Response response = await Dio().get(
        _login,
        queryParameters: params,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getUser(String userId) async {
    var params = {
      "userid": userId,
    };
    try {
      Response response = await Dio().get(
        _getUser,
        queryParameters: params,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getReferral(String userId) async {
    var params = {
      "userid": userId,
    };
    try {
      Response response = await Dio().get(
        _getReferral,
        queryParameters: params,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> editUser(String userId, String name, String username,
      String email, String nohp, String pass) async {
    FormData formData = new FormData.fromMap({
      "userid": userId,
      "name": name,
      "username": username,
      "email": email,
      "nohp": nohp,
      "password": pass,
    });
    try {
      Response response = await Dio().post(
        _editUser,
        data: formData,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getListChat(String userId) async {
    var params = {
      "userid": userId,
    };
    try {
      Response response = await Dio().get(
        _getListChat,
        queryParameters: params,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> lupaPassword(String email) async {
    var params = {
      "email": email,
    };
    try {
      Response response = await Dio().get(
        _lupaPassword,
        queryParameters: params,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getNewDetail(String userId) async {
    var params = {
      "userid": userId,
    };
    try {
      Response response = await Dio().get(
        _getNewsDetail,
        queryParameters: params,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> getTerm() async {
    var params = {};
    try {
      Response response = await Dio().get(_getTerm);
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }

  Future<dynamic> addMeetingRoom(String meeting_name, String meeting_password,userid) async {
    var params = {
      'name': meeting_name,
      'password': meeting_password,
      'userid': userid,
    };
    try {
      Response response = await Dio().get(
        _addMeetingRoom,
        queryParameters: params,
      );
      if (response.statusCode == 200) {
        var data = response.data;
        return data["data"];
      } else {
        return "Erro status code ${response.statusCode}";
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stacktrace: $stacktrace");
      return "Exception occured: $error stacktrace: $stacktrace";
    }
  }
}
