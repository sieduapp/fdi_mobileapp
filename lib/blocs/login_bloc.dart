import 'package:fdi/repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginBloc {
  final Repository _repository = Repository();

  Future<bool> login(String email, String password) async {
    dynamic response = await _repository.login(email, password);
    // _login.sink.add(response);
    if (response.length <= 0 || response == null) return false;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('IsAuthenticated', true);
    await prefs.setString('userid', response["user_id"]);
    await prefs.setString('email', email);

    return true;
  }

  dispose() {
    // _login.close();
  }

  // BehaviorSubject<dynamic> get subjectLogin => _login;
}

final loginBloc = LoginBloc();
