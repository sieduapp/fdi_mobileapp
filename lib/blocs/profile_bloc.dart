import 'package:fdi/repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<dynamic> _user = BehaviorSubject<dynamic>();
  final BehaviorSubject<dynamic> _referral = BehaviorSubject<dynamic>();

  getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString("userid");
    dynamic response = await _repository.getUser(userId);
    _user.sink.add(response);
  }

  Future<dynamic> getReferral() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString("userid");
    dynamic response = await _repository.getReferral(userId);
    _referral.sink.add(response);
  }

  editUser(String userid, String name, String username, String email,
      String nohp, String password) async {
    dynamic response = await _repository.editUser(
        userid, name, username, email, nohp, password);
    return response;
  }

  dispose() {
    _user.close();
    _referral.close();
  }

  BehaviorSubject<dynamic> get subjectUser => _user;
  BehaviorSubject<dynamic> get subjectReferral => _referral;
}

final profileBloc = ProfileBloc();
