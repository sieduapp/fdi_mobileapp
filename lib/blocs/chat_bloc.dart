import 'package:fdi/repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChatBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<dynamic> _chat = BehaviorSubject<dynamic>();

  getChatList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString("userid");
    dynamic response = await _repository.getListChat(userId);
    _chat.sink.add(response);
  }

  dispose() {
    _chat.close();
  }

  BehaviorSubject<dynamic> get chat => _chat;
}

final chatBloc = ChatBloc();
