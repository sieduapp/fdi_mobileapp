import 'package:fdi/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class GameBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<dynamic> _game = BehaviorSubject<dynamic>();

  getGameDetail(String gameid) async {
    dynamic response = await _repository.getgameDetail(gameid);
    _game.sink.add(response);
  }

  dispose() {
    _game.close();
  }

  BehaviorSubject<dynamic> get gameStream => _game;
}

final gameBloc = GameBloc();
