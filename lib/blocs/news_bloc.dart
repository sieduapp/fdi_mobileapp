import 'package:fdi/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class NewsBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<dynamic> _news = BehaviorSubject<dynamic>();

  getNewDetail(String id) async {
    dynamic response = await _repository.getNewDetail(id);
    _news.sink.add(response);
  }

  dispose() {
    _news.close();
  }

  BehaviorSubject<dynamic> get newStream => _news;
}

final newsBloc = NewsBloc();
