import 'package:fdi/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<String> _identityVideo = BehaviorSubject<String>();
  final BehaviorSubject<dynamic> _homeGames = BehaviorSubject<dynamic>();
  final BehaviorSubject<dynamic> _homeMeeting = BehaviorSubject<dynamic>();
  final BehaviorSubject<dynamic> _homeNews = BehaviorSubject<dynamic>();
  final BehaviorSubject<dynamic> _homeTerm = BehaviorSubject<dynamic>();

  getIdentityVideo() async {
    String response = await _repository.getIdentityVideo();
    _identityVideo.sink.add(response);
  }

  getHomeGames() async {
    dynamic response = await _repository.getHomeGames();
    _homeGames.sink.add(response);
  }

  getHomeMeeting() async {
    dynamic response = await _repository.getHomeMeeting();
    _homeMeeting.sink.add(response);
  }

  getHomeNews() async {
    dynamic response = await _repository.getHomeNews();
    _homeNews.sink.add(response);
  }

  getTerm() async {
    dynamic response = await _repository.getTerm();
    _homeTerm.sink.add(response);
  }

  dispose() {
    _identityVideo.close();
    _homeGames.close();
    _homeMeeting.close();
    _homeNews.close();
    _homeTerm.close();
  }

  BehaviorSubject<String> get identityVideo => _identityVideo;
  BehaviorSubject<dynamic> get homeGames => _homeGames;
  BehaviorSubject<dynamic> get homeMeeting => _homeMeeting;
  BehaviorSubject<dynamic> get homeNews => _homeNews;
  BehaviorSubject<dynamic> get homeTerm => _homeTerm;
}

final homeBloc = HomeBloc();
