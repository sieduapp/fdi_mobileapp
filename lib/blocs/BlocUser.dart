
import 'package:fdi/models/ModelUser.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class BlocUser extends ChangeNotifier{
  
  //Prototype get list data from json
  List<ModelUser> _user;
  List<ModelUser> get listUser => _user;
  set listUser(List<ModelUser> val){
    _user=val;
//    notifyListeners();
  }

  //Connection to api webservice database
  Future<List<ModelUser>> fecthData(params)async{
    print(params["data"]);
    final response = await http.get('https://jsonplaceholder.typicode.com/users/');
    List myData = modelUserFromJson(response.body);
    // print("MyData : " + myData.toString());
    listUser = myData;
    return listUser;
  }
}