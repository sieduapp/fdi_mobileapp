import 'package:fdi/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class VideoBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<String> _identityVideo = BehaviorSubject<String>();
  final BehaviorSubject<dynamic> _videosFeed = BehaviorSubject<dynamic>();
  final BehaviorSubject<dynamic> _videoDetail = BehaviorSubject<dynamic>();

  getIdentityVideo() async {
    String response = await _repository.getIdentityVideo();
    _identityVideo.sink.add(response);
  }

  getVideosFeed() async {
    dynamic response = await _repository.getVideosFeed();
    _videosFeed.sink.add(response);
  }

  getVideoDetail(String youtubeId) async {
    dynamic response = await _repository.getVideoDetail(youtubeId);
    _videoDetail.sink.add(response);
  }

  dispose() {
    _identityVideo.close();
    _videosFeed.close();
    _videoDetail.close();
  }

  BehaviorSubject<String> get identityVideo => _identityVideo;
  BehaviorSubject<dynamic> get videosFeed => _videosFeed;
  BehaviorSubject<dynamic> get videoDetail => _videoDetail;
}

final videoBloc = VideoBloc();
