import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyTextFormField extends StatelessWidget {
  final String label;
  final String hintText;
  final Function validator;
  final Function onSaved;
  final Function onChanged;
  final Widget onSuffix;
  final bool isPassword;
  final bool isEmail;

  MyTextFormField({
    this.label,
    this.hintText,
    this.validator,
    this.onSaved,
    this.onChanged,
    this.onSuffix,
    this.isPassword = false,
    this.isEmail = false,
  });

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: width * 0.01),
            child: TextFormField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide:
                    BorderSide(color: Colors.blue)),
                contentPadding: EdgeInsets.all(10.0),
                hintText: hintText,
              ),
            ),
          ),
          SizedBox(
            height: width * 0.05,
          ),
        ],
      ),
    );
//    return TextFormField(
//      decoration: new InputDecoration(
//        labelText: hintText,
//        fillColor: Colors.white,
//        border: new OutlineInputBorder(
////                  borderRadius: new BorderRadius.circular(25.0),
//          borderSide: new BorderSide(),
//        ),
//        suffixIcon: onSuffix,
//        //fillColor: Colors.green
//      ),
//      obscureText: isPassword ? true : false,
//      validator: validator,
//      onSaved: onSaved,
//      keyboardType: isEmail ? TextInputType.emailAddress : TextInputType.text,
//    );
  }
}

class MyButtonFormField extends StatelessWidget {
  final String label;
  final Function onSaved;

  MyButtonFormField({
    this.label,
    this.onSaved,
  });

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              child: ButtonTheme(
                minWidth: width,
                height: width * 0.11,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(color: Colors.transparent)),
                  onPressed: onSaved,
                  color: Colors.blue,
                  textColor: Colors.white,
                  child: Text(label),
                ),
              )),
          SizedBox(
            height: width * 0.10,
          ),
        ],
      ),
    );
  }
}
