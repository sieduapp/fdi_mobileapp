import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyMenuImage extends StatelessWidget {
  final String image;
  final String label;
  final Function navigator;

  MyMenuImage({
    this.image,
    this.label,
    this.navigator,
  });

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        ButtonTheme(
          minWidth: width * 0.15,
          height: width * 0.15,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Image(
                image: AssetImage(this.image),
                height: width * 0.07,
                width: width * 0.08,
                fit: BoxFit.contain),
            color: Colors.white,
            onPressed: this.navigator,
          ),
        ),
        SizedBox(
          height: width * 0.02,
        ),
        Text(this.label)
      ],
    );
  }
}
