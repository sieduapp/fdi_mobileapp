import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum DialogAction { yes, cancel }

class MyAlertDialog {
  static Future<DialogAction> dialog(BuildContext context, String title, List body, List buttonAction) async {
    final action = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(title),
              content: SingleChildScrollView(
                child: ListBody(
                  children: body
                ),
              ),
              actions: buttonAction);
        });
  }
}
