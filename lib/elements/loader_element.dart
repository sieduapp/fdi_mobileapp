import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

Widget buildLoadingWidget() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.white,
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
        ),
      ),
    ],
  );
}
