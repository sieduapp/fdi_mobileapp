class Chat {
  final String date;
  final String halaman;
  final String hours;

  Chat({this.date, this.halaman, this.hours});

  factory Chat.fromJson(Map<dynamic, dynamic> json) {
    String parser(dynamic source) {
      try {
        return parser(source.toString());
      } on FormatException {
        return '';
      }
    }

    return Chat(
        date: parser(json['date']),
        halaman: parser(json['halaman']),
        hours: parser(json['hours']));
  }
}
